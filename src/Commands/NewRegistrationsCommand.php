<?php

namespace Redenge\OnlineUser\Commands;

use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\Entity\Repository\DomainRepository;
use Redenge\OnlineUser\FrontModule\Entity\Repository\SettingsProfileRepository;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\ProviderContainer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Description of NewRegistrationsCommand
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class NewRegistrationsCommand extends Command
{

	/**
	 * @var AppFactory
	 */
	protected $appFactory;

	/**
	 * @var ProviderContainer
	 */
	protected $providerContainer;

	/**
	 * @var SettingsProfileRepository
	 */
	protected $settingsProfileRepository;

	/**
	 * @var DomainRepository
	 */
	protected $domainRepository;

	/**
	 * Ukládání URL, které jsme už otestovali na nové registrace
	 *
	 * @var array 
	 */
	private $processedApiUrl = [];


	public function __construct(
		AppFactory $appFactory,
		ProviderContainer $providerContainer,
		SettingsProfileRepository $settingsProfileRepository,
		DomainRepository $domainRepository
	)
	{
		parent::__construct();

		$this->appFactory = $appFactory;
		$this->providerContainer = $providerContainer;
		$this->settingsProfileRepository = $settingsProfileRepository;
		$this->domainRepository = $domainRepository;
	}


	protected function configure()
	{
		$this->setName('onlineUser:sendEmailToNewRegistrations')
			->setDescription('Send activation email to new registrations');
	}


	protected function execute(InputInterface $input, OutputInterface $output)
	{
		foreach ($this->providerContainer->getProviders() as $provider) {
			$this->sendEmails(
				$input,
				$output,
				$provider->getEnvironmentKey()->getMultishopCode(),
				$provider->getEnvironmentKey()->getProfileCode(),
				$this->settingsProfileRepository->getDefaultCountry($provider->getEnvironmentKey()->getProfileCode())
			);
		}
	}


	private function sendEmails(InputInterface $input, OutputInterface $output, $multishopCode, $profileCode, $countryIso)
	{
		$app = $this->appFactory->create(
			EnvironmentKeyFactory::create($multishopCode, $profileCode));
		$appUrl = $app->getProvider()->getAppUrl();
		if (in_array($appUrl, $this->processedApiUrl)) {
			return;
		}
		$this->processedApiUrl[] = $appUrl;

		$output->writeln(sprintf('<comment>%s | Checking registrations for multishop=%s, profile=%s, country=%s</comment>', date('j.n.Y H:i:s'), $multishopCode, $profileCode, $countryIso));

		$registrations = $app->getNewRegistrations();
		if (empty($registrations)) {
			$output->writeln(sprintf('<comment>%s | No more registrations</comment>', date('j.n.Y H:i:s')));
			return 0;
		}

		$emailService = $app->getEmailService();
		$emailService->setEnvironment($multishopCode, $countryIso);
		$emailService->getLinker()->setUrl($this->domainRepository->getUrlScript($multishopCode, $profileCode));

		/*
		 * Ukládám id vztahů, na které se odeslal email
		 * Tyto id vztahů pak zasílám pro potvrzení do API, že jsem aktivační email v pořádku odeslal a API mi tím pádem
		 * nemusí posílat znovu tento email
		 */
		$confirmIds = [];

		/*
		 * Z HeO se může vracet více vztahů se stejným emailem. Musíme zajistit, aby zákazníkovi přišel jen jeden email
		 */
		$sendedEmails = [];
		foreach ($registrations as $registration) {
			if (!isset($registration['Email']) && !isset($registration['DoubleOptInHash']) && !isset($registration['ID'])) {
				continue;
			}
			try {
				if (!in_array($registration['Email'], $sendedEmails)) {
					$emailService->sendConfirmRegistrationHeOEmail($registration['Email'], $registration['DoubleOptInHash']);
					$sendedEmails[] = $registration['Email'];
					$output->writeln(sprintf('<info>%s | Activation email is succesfully send to ,,%s"</info>', date('j.n.Y H:i:s'), $registration['Email']));
				}
				$confirmIds[] = $registration['ID'];
			} catch (Exception $e) {
				$output->writeln(sprintf('<error>%s | Error during sending activation email to ,,%s"</error>', date('j.n.Y H:i:s'), $registration['Email']));
			}
		}

		if (!empty($confirmIds)) {
			$app->confirmNewRegistrations($confirmIds);
		}
	}
}
