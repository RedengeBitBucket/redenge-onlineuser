<?php

namespace Redenge\OnlineUser\FrontModule\HttpClients;

use Exception;
use Redenge\OnlineUser\FrontModule\Http\RawResponse;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;


/**
 * Description of GuzzleHttpClient
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class GuzzleHttpClient implements HttpClientInterface
{

	/**
	 * @var \GuzzleHttp\Client The Guzzle client.
	 */
	protected $guzzleClient;


	/**
	 * @param \GuzzleHttp\Client|null The Guzzle client.
	 */
	public function __construct(Client $guzzleClient = null)
	{
		$this->guzzleClient = $guzzleClient ? : new Client();
	}


	/**
	 * @inheritdoc
	 */
	public function send($url, $method, array $body, array $headers, $timeOut)
	{
		$options = [
			'headers' => $headers,
			'json' => $body,
			'timeout' => $timeOut,
			'connect_timeout' => 5,
			'verify' => false
		];

		try {
			$rawResponse = $this->guzzleClient->request($method, $url, $options);
		} catch (ConnectException $e) {
			throw new Exception('Technický problém. Kontaktujte prosím podporu');
		} catch (RequestException $e) {
			$rawResponse = $e->getResponse();
		}
		
		$rawHeaders = $this->getHeadersAsString($rawResponse);
		$rawBody = $rawResponse->getBody();
		$httpStatusCode = $rawResponse->getStatusCode();

		return new RawResponse($rawHeaders, $rawBody, $httpStatusCode);
	}


	/**
	 * Returns the Guzzle array of headers as a string.
	 *
	 * @param ResponseInterface|null $response The Guzzle response.
	 *
	 * @return string
	 */
	public function getHeadersAsString($response)
	{
		$headers = $response->getHeaders();
		$rawHeaders = [];
		foreach ($headers as $name => $values) {
			$rawHeaders[] = $name . ": " . implode(", ", $values);
		}

		return implode("\r\n", $rawHeaders);
	}

}
