<?php

namespace Redenge\OnlineUser\FrontModule\Entity;

use Nette;
use Redenge\Application\Entity\Entity;


/**
 * Description of Address
 *
 * @property string   $vat
 * @property string   $euvat
 * @property bool     isDefaultDeliveryAddress
 * @property int      $organizationNumber
 * @property int      $priceLevel
 * @property string   $name
 * @property string   $street
 * @property string   $houseNumber
 * @property string   $orientationNumber
 * @property string   $city
 * @property string   $zipPostalCode
 * @property string   $country
 * @property bool     $isBilling
 * 
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Address extends Entity
{

	use Nette\SmartObject;

	const PAYMENT_TRANSFER = 'Platebním příkazem';

	const PAYMENT_TRANSFER_SK = 'Platobný príkaz';

	const PAYMENT_CASH_STORE = 'V hotovosti na predajni';

	/**
	 * @var string
	 */
	protected $vat;

	/**
	 * @var string
	 */
	protected $euvat;

	/**
	 * @var bool
	 */
	protected $isDefaultDeliveryAddress;

	/**
	 * @var int
	 */
	protected $organizationNumber;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $street;

	/**
	 * @var string
	 */
	protected $houseNumber;

	/**
	 * @var string
	 */
	protected $orientationNumber;

	/**
	 * @var string
	 */
	protected $city;

	/**
	 * @var string
	 */
	protected $zipPostalCode;

	/**
	 * @var string
	 */
	protected $country;

	/**
	 * @var bool
	 */
	protected $isBilling;

	/**
	 * @var array
	 */
	protected $addAdds;


	/**
	 * @return string
	 */
	public function getVat()
	{
		return $this->vat;
	}


	/**
	 * @return string
	 */
	public function getEuvat()
	{
		return $this->euvat;
	}


	/**
	 * @return bool
	 */
	public function isDefaultDeliveryAddress()
	{
		return $this->isDefaultDeliveryAddress;
	}


	/**
	 * @return int
	 */
	public function getOrganizationNumber()
	{
		return $this->organizationNumber;
	}


	/**
	 * @return int
	 */
	public function getPriceLevel()
	{
		return $this->priceLevel;
	}


	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * @return string
	 */
	public function getStreet()
	{
		return $this->street;
	}


	/**
	 * @return string
	 */
	public function getHouseNumber()
	{
		return $this->houseNumber;
	}


	/**
	 * @return string
	 */
	public function getOrientationNumber()
	{
		return $this->orientationNumber;
	}


	/**
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}


	/**
	 * @return string
	 */
	public function getZipPostalCode()
	{
		return $this->zipPostalCode;
	}


	/**
	 * @return string
	 */
	public function getCountry()
	{
		return $this->country;
	}


	/**
	 * @return bool
	 */
	public function getIsBilling()
	{
		return $this->isBilling;
	}


	/**
	 * @return array
	 */
	public function getAddAdds()
	{
		return $this->addAdds;
	}


	/**
	 * @return string
	 */
	public function getEuVatSk()
	{
		foreach ($this->addAdds as $add) {
			if ($add['Name'] !== 'EUVATSK') {
				continue;
			}

			return $add['Value'];
		}

		return '';
	}


	public function getPayment()
	{
		foreach ($this->addAdds as $add) {
			if ($add['Name'] !== 'PrednastavenaFormaUhrady') {
				continue;
			}

			return $add['Value'];
		}

		return '';
	}


	/**
	 * Má zákazník nastavenou platbu převodem?
	 *
	 * @return bool
	 */
	public function isPaymentTransfer()
	{
		$payment = $this->getPayment();
		return $payment === self::PAYMENT_TRANSFER || $payment === self::PAYMENT_TRANSFER_SK;
	}


	/**
	 * Má zákazník nastavenou platbu hotově na prodejně?
	 *
	 * @return bool
	 */
	public function isPaymentCashStore()
	{
		return $this->getPayment() === self::PAYMENT_CASH_STORE;
	}


	/**
	 * @param string $vat
	 */
	public function setVat($vat)
	{
		$this->vat = $vat;
	}


	/**
	 * @param string $euvat
	 */
	public function setEuvat($euvat)
	{
		$this->euvat = $euvat;
	}


	/**
	 * @param bool $isDefaultDeliveryAddress
	 */
	public function setIsDefaultDeliveryAddress($isDefaultDeliveryAddress)
	{
		$this->isDefaultDeliveryAddress = (bool) $isDefaultDeliveryAddress;
	}


	/**
	 * @param int $organizationNumber
	 */
	public function setOrganizationNumber($organizationNumber)
	{
		$this->organizationNumber = (int) $organizationNumber;
	}


	/**
	 * @param int $priceLevel
	 */
	public function setPriceLevel($priceLevel)
	{
		$this->priceLevel = (int) $priceLevel;
	}


	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}


	/**
	 * @param string $street
	 */
	public function setStreet($street)
	{
		$this->street = $street;
	}


	/**
	 * @param string $houseNumber
	 */
	public function setHouseNumber($houseNumber)
	{
		$this->houseNumber = $houseNumber;
	}


	/**
	 * @param string $orientationNumber
	 */
	public function setOrientationNumber($orientationNumber)
	{
		$this->orientationNumber = $orientationNumber;
	}


	/**
	 * @param string $city
	 */
	public function setCity($city)
	{
		$this->city = $city;
	}


	/**
	 * @param string $zipPostalCode
	 */
	public function setZipPostalCode($zipPostalCode)
	{
		$this->zipPostalCode = $zipPostalCode;
	}


	/**
	 * @param string $country
	 */
	public function setCountry($country)
	{
		$this->country = $country;
	}


	/**
	 * @param string $isBilling
	 */
	public function setIsBilling($isBilling)
	{
		$this->isBilling = $isBilling;
	}


	/**
	 * @param array $addAdds
	 */
	public function setAddAdds($addAdds)
	{
		$this->addAdds = $addAdds;
	}

}
