<?php

namespace Redenge\OnlineUser\FrontModule\Entity;

use Nette;
use Nette\Security\IIdentity;
use Redenge\Application\Entity\Entity;


/**
 * Description of User
 * 
 * @property int        $id
 * @property string     $name
 * @property string     $surname
 * @property string     $degreeBefore
 * @property string     $degreeAfter
 * @property string     $email
 * @property string     $organizationName
 * @property string     $phone
 * @property string     $mobile
 * @property string     $fax
 * @property string     $role
 * @property string     $function
 * @property int        $invoiceOrganizationNumber
 * @property int        $organizationNumber
 * @property int        $priceLevel
 * @property bool       $eshopCustomer
 * @property int        $contactNumber
 * @property Address    $address
 * @property Address    $deliveryAddress
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class User extends Entity implements IIdentity
{

	use Nette\SmartObject;

	const ROLE_SUPER_ADMIN = 'Superadmin';
	const ROLE_ADMIN = 'Administrator';

	/**
	 * @var string
	 */
	protected $email;


	/**
	 * @var bool
	 */
	protected $eshopCustomer;

	/**
	 * @var int
	 */
	protected $contactNumber;

	/**
	 * @var int
	 */
	protected $priceLevel;

	/**
	 * @var string
	 */
	protected $organizationName;

	/**
	 * @var int
	 */
	protected $organizationNumber;

	/**
	 * @var int
	 */
	protected $invoiceOrganizationNumber;

	/**
	 * @var string
	 */
	protected $degreeBefore;

	/**
	 * @var string
	 */
	protected $degreeAfter;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $surname;

	/**
	 * @var string
	 */
	protected $phone;

	/**
	 * @var string
	 */
	protected $mobile;

	/**
	 * @var string
	 */
	protected $fax;

	/**
	 * @var string
	 */
	protected $role;

	/**
	 * @var string
	 */
	protected $function;

	/**
	 * @var Address
	 */
	protected $address;

	/**
	 * @var Address
	 */
	protected $deliveryAddress;


	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}


	/**
	 * @return bool
	 */
	public function getEshopCustomer()
	{
		return $this->eshopCustomer;
	}


	/**
	 * @return int
	 */
	public function getContactNumber()
	{
		return $this->contactNumber;
	}


	/**
	 * @return int
	 */
	public function getPriceLevel()
	{
		return $this->priceLevel;
	}


	/**
	 * @return string
	 */
	public function getOrganizationName()
	{
		return $this->organizationName;
	}


	/**
	 * @return int
	 */
	public function getOrganizationNumber()
	{
		return $this->organizationNumber;
	}


	/**
	 * @return int
	 */
	public function getInvoiceOrganizationNumber()
	{
		return $this->invoiceOrganizationNumber;
	}


	/**
	 * @return string
	 */
	public function getDegreeBefore()
	{
		return $this->degreeBefore;
	}


	/**
	 * @return string
	 */
	public function getDegreeAfter()
	{
		return $this->degreeAfter;
	}


	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * @return string
	 */
	public function getSurname()
	{
		return $this->surname;
	}


	/**
	 * @return string
	 */
	public function getPhone()
	{
		return $this->phone;
	}


	/**
	 * @return string
	 */
	public function getMobile()
	{
		return $this->mobile;
	}


	/**
	 * @return string
	 */
	public function getFax()
	{
		return $this->fax;
	}


	/**
	 * @return string
	 */
	public function getRole()
	{
		return $this->role;
	}


	/**
	 * @return string
	 */
	public function getFunction()
	{
		return $this->function;
	}


	/**
	 * @return Address
	 */
	public function getAddress()
	{
		return $this->address;
	}


	/**
	 * @return Address
	 */
	public function getDeliveryAddress()
	{
		return $this->deliveryAddress;
	}


	/**
	 * @param string $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}


	/**
	 * @param bool $eshopCustomer
	 */
	public function setEshopCustomer($eshopCustomer)
	{
		$this->eshopCustomer = (bool) $eshopCustomer;
	}


	/**
	 * @param int $contactNumber
	 */
	public function setContactNumber($contactNumber)
	{
		$this->contactNumber = (int) $contactNumber;
	}


	/**
	 * @param int $priceLevel
	 */
	public function setPriceLevel($priceLevel)
	{
		$this->priceLevel = (int) $priceLevel;
	}


	/**
	 * @param string $organizationName
	 */
	public function setOrganizationName($organizationName)
	{
		$this->organizationName = $organizationName;
	}


	/**
	 * @param int $organizationNumber
	 */
	public function setOrganizationNumber($organizationNumber)
	{
		$this->organizationNumber = (int) $organizationNumber;
	}


	/**
	 * @param int $invoiceOrganizationNumber
	 */
	public function setInvoiceOrganizationNumber($invoiceOrganizationNumber)
	{
		$this->invoiceOrganizationNumber = (int) $invoiceOrganizationNumber;
	}


	/**
	 * @param string $degreeBefore
	 */
	public function setDegreeBefore($degreeBefore)
	{
		$this->degreeBefore = $degreeBefore;
	}


	/**
	 * @param string $degreeAfter
	 */
	public function setDegreeAfter($degreeAfter)
	{
		$this->degreeAfter = $degreeAfter;
	}


	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}


	/**
	 * @param string $surname
	 */
	public function setSurname($surname)
	{
		$this->surname = $surname;
	}


	/**
	 * @param string $phone
	 */
	public function setPhone($phone)
	{
		$this->phone = $phone;
	}


	/**
	 * @param string $mobile
	 */
	public function setMobile($mobile)
	{
		$this->mobile = $mobile;
	}


	/**
	 * @param string $fax
	 */
	public function setFax($fax)
	{
		$this->fax = $fax;
	}


	/**
	 * @param string $role
	 */
	public function setRole($role)
	{
		$this->role = $role;
	}


	/**
	 * @param string $function
	 */
	public function setFunction($function)
	{
		$this->function = $function;
	}


	public function setAddress(Address $address)
	{
		$this->address = $address;
	}


	public function setDeliveryAddress(Address $address)
	{
		$this->deliveryAddress = $address;
	}


	/**
	 * {@inheritdoc}
	 */
	public function getRoles()
	{
		return [$this->role];
	}

}
