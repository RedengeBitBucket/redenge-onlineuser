<?php

namespace Redenge\OnlineUser\FrontModule\Entity\Repository;

use Nette\Database\Context;


/**
 * Description of AbstractRepository
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
abstract class AbstractRepository
{

	/**
	 * @var Context
	 */
	protected $db;


	public function injectConnection(Context $db)
	{
		$this->db = $db;
	}

}
