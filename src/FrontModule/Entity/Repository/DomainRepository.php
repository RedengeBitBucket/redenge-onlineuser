<?php

namespace Redenge\OnlineUser\FrontModule\Entity\Repository;

use Nette\Http\UrlScript;


/**
 * Description of DomainRepository
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class DomainRepository extends AbstractRepository
{

	/**
	 * 
	 * @param string $multishopCode
	 * @param string $profileCode
	 * @return string
	 */
	public function getDomainName($multishopCode, $profileCode)
	{
		$return = $this->db->query(
			"SELECT `name` FROM domain"
			. " JOIN multishop_domain md ON md.id_domain = domain.id"
			. " AND md.id_settings_profile=(SELECT id FROM settings_profile WHERE `code`= ?)"
			. " AND md.id_multishop =(SELECT id FROM multishop WHERE `code`=?)"
			. " AND main=1", $profileCode, $multishopCode)
			->fetchField('name');

		return $return ? $return : '';
	}


	/**
	 * 
	 * @param string $multishopCode
	 * @param string $profileCode
	 * @return UrlScript
	 */
	public function getUrlScript($multishopCode, $profileCode)
	{
		$url = new UrlScript();
		$url->setHost($this->getDomainName($multishopCode, $profileCode));
		if (FORCE_HTTPS) {
			$url->setScheme('https');
		} else {
			$url->setScheme('http');
		}

		return $url;
	}

}
