<?php

namespace Redenge\OnlineUser\FrontModule\Entity\Repository;

/**
 * Description of SettingsProfileRepository
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class SettingsProfileRepository extends AbstractRepository
{

	public function getDefaultCountry($profileCode)
	{
		return $this->db->query(
			"SELECT country.code FROM country"
			. " JOIN settings_profile sp ON sp.id_country = country.id"
			. " AND sp.`code` = ? LIMIT 1", $profileCode)
			->fetchField('code');
	}

}
