<?php

namespace Redenge\OnlineUser\FrontModule\Entity\Repository;

use Nette\Utils\Strings;
use Kdyby\Translation\Translator;


/**
 * Description of CountryRepository
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CountryRepository extends AbstractRepository
{

	public function __construct(Translator $translator)
	{
		$this->translator = $translator;
	}


	/**
	 * @return array
	 */
	public function getMobilePrefixes()
	{
		$return = [];
		foreach ($this->db->table('country')->select('code, antenumber')->where('antenumber > 0')->fetchAssoc('code=antenumber') as $countryIso => $value) {
			if (!Strings::startsWith('+', $value)) {
				$value = '+' . $value;
			}
			$return[strtoupper($countryIso)] = $value;
		}

		return $return;
	}


	public function formatMobilePrefixes($data = [])
	{
		$return = [];
		if (empty($data)) {
			$data = $this->getMobilePrefixes();
		}

		foreach ($data as $countryIso => $antenumber) {
			$return[$antenumber] = $antenumber;
		}

		return $return;
	}


	/**
	 * Vrátí země, které jsou přiřazené ke konkrétnímu profilu
	 *
	 * @param string $profileCode
	 * @return array [code => name]
	 */
	public function getAssignedCountries($profileCode)
	{
		$return = [];
		$countries =  $this->db->query(
			"SELECT `code` FROM country"
			. " JOIN settings_profile_country spc ON spc.id_country = country.id"
			. " AND spc.id_settings_profile = (SELECT id FROM settings_profile WHERE `code` = ?)", $profileCode)
			->fetchPairs('code', 'code');
		foreach ($countries as $countryIso) {
			$return[$countryIso] = $this->translator->translate('country.' .strtoupper($countryIso));
		}

		return $return;
	}


	/**
	 * @param string $countryIso
	 * @return int
	 */
	public function getCountryId($countryIso)
	{
		return $this->db->table('country')->where('UPPER(`code`) = ?', strtoupper($countryIso))->fetchField('id');
	}

}
