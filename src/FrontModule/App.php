<?php

namespace Redenge\OnlineUser\FrontModule;

use InvalidArgumentException;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Http\Session;
use Redenge\OnlineUser\FrontModule\Exceptions\ApiRedengeException;
use Redenge\OnlineUser\FrontModule\Mail\IMail;
use Redenge\OnlineUser\FrontModule\Settings;
use Redenge\OnlineUser\FrontModule\Tools\FormatHelper;


/**
 * Description of App
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class App
{

	const APP_URL_ENV_NAME = 'APP_URL';
	const APP_USERNAME_ENV_NAME = 'APP_USERNAME';
	const APP_PASSWORD_ENV_NAME = 'APP_PASSWORD';
	const REDIRECT_URL = '/';

	const CACHE_NS = 'Redenge.OnlineUser.App';
	const CACHE_EXPIRE = '3 minutes';
	const CACHE_TAG_LOGIN = 'login';
	const CACHE_TAG_GET_CONTACT_PERSON = 'getContactPerson';
	const CACHE_TAG_GET_DELIVERY_ADDRESSES = 'getDeliveryAddresses';
	const CACHE_TAG_GET_CONTACT_PERSONS_FOR_INVOICE_ORGANIZATION_BY_ADMIN = 'getContactPersonsForInvoiceOrganizationByAdmin';

	const CONNECTION_TYPE_PHONE = 'phone';
	const CONNECTION_TYPE_MOBILE = 'mobile';

	/**
	 * @var Settings
	 */
	protected $settings;

	/**
	 * @var Provider
	 */
	protected $provider;

	/**
     * @var Client The client service.
     */
    protected $client;

	/**
	 * @var Response|null Stores the last request.
	 */
	protected $lastResponse;

	/**
	 * @var Cache
	 */
	protected $cache;

	/**
	 * @var Session
	 */
	protected $session;

	/**
	 * @var IMail
	 */
	protected $emailService;


	/**
	 * Instantiates a new OnlineUser super-class object.
	 *
	 * @param Settings $settings
	 * @param Client $client
	 * @param IStorage $storage
	 * @param Session $session
	 * @param IMail $emailService
	 *
	 * @throws ApiRedengeException
	 */
	public function __construct(
		Settings $settings,
		Provider $provider,
		Client $client,
		IStorage $storage,
		Session $session,
		IMail $emailService
	)
	{
		if (!$provider->getAppUrl()) {
			throw new ApiRedengeException('Required "app_url" key not supplied in config and could not find fallback environment variable "' . static::APP_URL_ENV_NAME . '"');
		}
		if (!$provider->getAppUsername()) {
			throw new ApiRedengeException('Required "app_username" key not supplied in config and could not find fallback environment variable "' . static::APP_USERNAME_ENV_NAME . '"');
		}
		if (!$provider->getAppPassword()) {
			throw new ApiRedengeException('Required "app_password" key not supplied in config and could not find fallback environment variable "' . static::APP_PASSWORD_ENV_NAME . '"');
		}
		$this->settings = $settings;
		$this->provider = $provider;
		$this->client = $client;
		if ($settings->getHttpClientHandler() !== null) {
			$this->client->setHttpClientHandler($settings->getHttpClientHandler());
		}
		$this->cache = new Cache($storage, self::CACHE_NS);
		$this->session = $session;
		$this->emailService = $emailService;
	}


	/**
	 * @return Cache
	 */
	public function getCache()
	{
		return $this->cache;
	}


	/**
	 * @return IMail
	 */
	public function getEmailService()
	{
		return $this->emailService;
	}


	/**
	 * @return string
	 */
	public function getSettings()
	{
		return $this->settings;
	}


	public function getProvider()
	{
		return $this->provider;
	}


	/**
	 * Sends a POST request and returns the result.
	 *
	 * @param string                  $endpoint
	 * @param array                   $params
	 *
	 * @return Response
	 *
	 * @throws ApiRedengeException
	 */
	public function post($endpoint, array $params = [])
	{
		return $this->sendRequest('POST', $endpoint, $params);
	}


	/**
	 * Sends a GET request and returns the result.
	 *
	 * @param string                  $endpoint
	 *
	 * @return Response
	 *
	 * @throws ApiRedengeException
	 */
	public function get($endpoint)
	{
		return $this->sendRequest('GET', $endpoint, $params = []);
	}


	/**
	 * Sends a request and returns the result.
	 *
	 * @param string                  $method
	 * @param string                  $endpoint
	 * @param array                   $params
	 *
	 * @return Response
	 *
	 * @throws ApiRedengeException
	 */
	public function sendRequest($method, $endpoint, array $params = [])
	{
		$request = $this->request($method, $endpoint, $params);

		return $this->lastResponse = $this->client->sendRequest($request);
	}


	/**
	 * Instantiates a new Request entity.
	 *
	 * @param string                  $method
	 * @param string                  $endpoint
	 * @param array                   $params
	 *
	 * @return Request
	 *
	 * @throws ApiRedengeException
	 */
	public function request($method, $endpoint, array $params = [])
	{
		return new Request(
			$this, $method, $endpoint, $params
		);
	}


	private function generateCacheKey($functionName, $args)
	{
		$key = [$this->session->getId(), $functionName];
		$key = array_merge($key, $args);

		return $key;
	}


	/**
	 * Přihlášení zákazníka
	 *
	 * @param string $email
	 * @param string $password
	 * @return Response
	 */
	public function login($email, $password)
	{
		$key = $this->generateCacheKey(__FUNCTION__, func_get_args());

		$response = $this->cache->load($key);
		if ($response === null) {
			$response = $this->post('/api/login', [
				'Email' => $email,
				'Password' => $password
			]);
			
			$this->cache->save($key, $response, [
				Cache::EXPIRE => self::CACHE_EXPIRE,
				Cache::TAGS => [self::CACHE_TAG_LOGIN],
			]);
		}

		return $response;
	}


	/**
	 * Přihlášení přes superadmina
	 *
	 * @param string $email
	 * @return Response
	 */
	public function superAdminLogin($email)
	{
		$key = $this->generateCacheKey(__FUNCTION__, func_get_args());

		$response = $this->cache->load($key);
		if ($response === null) {
			$response = $this->post('/api/superadminlogin', [
				'Email' => $email,
			]);
			
			$this->cache->save($key, $response, [
				Cache::EXPIRE => self::CACHE_EXPIRE,
				Cache::TAGS => [self::CACHE_TAG_LOGIN],
			]);
		}

		return $response;
	}


	public function getContactPerson($id)
	{
		$key = $this->generateCacheKey(__FUNCTION__, func_get_args());

		$body = $this->cache->load($key);
		if ($body === null) {
			$body = $this->post('/api/getcontactperson', [
				'ID' => $id,
			])->getDecodedBody();

			$this->cache->save($key, $body, [
				Cache::EXPIRE => self::CACHE_EXPIRE,
				Cache::TAGS => [self::CACHE_TAG_GET_CONTACT_PERSON],
			]);
		}

		return $body;
	}


	public function getInvoiceOrganizations($ids)
	{
		$key = $this->generateCacheKey(__FUNCTION__, func_get_args());

		$body = $this->cache->load($key);
		if ($body === null) {
			$body = $this->post('/api/getinvoiceorganizations', [
				'IDs' => $ids,
			])->getDecodedBody();

			$this->cache->save($key, $body, [
				Cache::EXPIRE => self::CACHE_EXPIRE,
			]);
		}

		return $body;
	}


	public function getContactPersonsForInvoiceOrganization($id, $email)
	{
		$key = $this->generateCacheKey(__FUNCTION__, func_get_args());

		$body = $this->cache->load($key);
		if ($body === null) {
			$body = $this->post('/api/getcontactpersonsforinvoiceorganization', [
				'InvoiceOrganizationNumber' => $id,
				'Email' => $email
			])->getDecodedBody();

			$this->cache->save($key, $body, [
				Cache::EXPIRE => self::CACHE_EXPIRE,
			]);
		}

		return $body;		
	}


	/**
	 * 
	 * @param type $id
	 */
	public function getContactPersonsForInvoiceOrganizationByAdmin($id)
	{
		$key = $this->generateCacheKey(__FUNCTION__, func_get_args());

		$body = $this->cache->load($key);
		if ($body === null) {
			$body = $this->post('/api/getcontactpersonsforinvoiceorganizationbyadmin', [
				'InvoiceOrganizationNumber' => $id
			])->getDecodedBody();

			$this->cache->save($key, $body, [
				Cache::EXPIRE => self::CACHE_EXPIRE,
				Cache::TAGS => [self::CACHE_TAG_GET_CONTACT_PERSONS_FOR_INVOICE_ORGANIZATION_BY_ADMIN],
			]);
		}

		return $body;
	}


	/**
	 * @param string $email
	 * @param string $password
	 * @return array
	 */
	public function changePassword($email , $password)
	{
		return $this->post('/api/changepassword', [
				'Email' => $email,
				'Password' => $password
			])->getDecodedBody();
	}


	/**
	 * @param string $hash
	 * @return array
	 */
	public function hashVerification($hash)
	{
		return $this->post('/api/hashverification', [
				'Hash' => $hash,
			])->getDecodedBody();
	}


	/**
	 * Ověření existence emailu.
	 * Vrací pole, které obsahuje informaci, zda emailu existuje + u kolika vztahů je přiřazen
	 *
	 * @param string $email
	 * @return array
	 */
	public function emailVerification($email)
	{
		$key = $this->generateCacheKey(__FUNCTION__, func_get_args());

		$body = $this->cache->load($key);
		if ($body === null) {
			$body = $this->post('/api/emailverification', [
				'Email' => $email,
			])->getDecodedBody();

			$this->cache->save($key, $body, [
				Cache::EXPIRE => self::CACHE_EXPIRE,
			]);
		}

		return $body;
	}


	/**
	 * Ověření existence emailu
	 * Vrací pouze informaci, zda existuje nebo ne
	 *
	 * @param string $email
	 * @return bool
	 */
	public function emailExists($email)
	{
		$body = $this->emailVerification($email);
		if (isset($body['Exists']) && $body['Exists'] === true) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Ověření, zda dané IČ existuje
	 *
	 * @param string $ic
	 * @return bool
	 */
	public function existsIc($ic)
	{
		$key = $this->generateCacheKey(__FUNCTION__, func_get_args());

		$body = $this->cache->load($key);
		if ($body === null) {
			$body = $this->post('/api/existstvat', [
				'VAT' => $ic,
			])->getDecodedBody();

			$this->cache->save($key, $body, [
				Cache::EXPIRE => self::CACHE_EXPIRE,
			]);
		}

		if (isset($body['Exists']) && $body['Exists'] === true) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Registrace nového zákazníka
	 *
	 * @param string $email
	 * @param string $name
	 * @param string $lastname
	 * @param string $mobile
	 * @param string $street
	 * @param string $streetNumber
	 * @param string $city
	 * @param string $zip
	 * @param string $country
	 * @param string $title_prefix
	 * @param string $title_suffix
	 * @param string $phone
	 * @param string $ic
	 * @param string $dic
	 * @param string $companyName
	 * @param string $orientation_number
	 */
	public function newRegistration($email, $name, $lastname, $mobile, $street, $streetNumber, $city, $zip, $country,
		$title_prefix = '', $title_suffix = '', $phone = '', $ic = '', $dic = '', $icDph = '', $companyName = '',
		$orientation_number = '')
	{
		$params = [];
		if (!empty($icDph)) {
			$params[] = $icDph;
		}
		if (empty($companyName)) {
			$companyName = $name . ' ' . $lastname;
		}
		$body = $this->post('/api/newregistration', [
			'Email' => $email,
			'DegreeBefore' => $title_prefix,
			'DegreeAfter' => $title_suffix,
			'Name' => $name,
			'Surname' => $lastname,
			'Phone' => $phone,
			'Mobile' => $mobile,
			'OrganizationName' => $companyName,
			'Street' => $street,
			'HouseNumber' => $streetNumber,
			'OrientationNumber' => $orientation_number,
			'City' => $city,
			'ZipPostalCode' => FormatHelper::formatZip($zip),
			'Country' => $country,
			'VAT' => $ic,
			'EUVAT' => $dic,
			'Function' => 'Nákup',
			'Role' => 'Běžný uživatel',
			'Params' => $params
		]);

		$this->emailService->sendConfirmRegistrationEmail($email, $body->getHash());
	}


	public function createNewDeliveryAddress($organizationNumber, $customerNumber, $name, $street, $city, $country,
		$zip, $isDefault, $email, $phone, $mobile, $houseNumber = '', $orientationNumber = '', $fax = '')
	{
		$body = $this->post('/api/createnewdeliveryaddress', [
			'OrganizationNumber' => $organizationNumber,
			'CustomerNumber' => $customerNumber,
			'Name' => $name,
			'Street' => $street,
			'City' => $city,
			'HouseNumber' => $houseNumber,
			'OrientationNumber' => $orientationNumber,
			'ZipPostalCode' => FormatHelper::formatZip($zip),
			'Country' => $country,
			'IsDefaultRelation' => $isDefault,
			'Email' => $email,
			'Phone' => $phone,
			'Mobile' => $mobile,
			'Fax' => $fax,
		])->decodeBody();

		return $body;
	}


	public function createNewUser($organizationNumber, $name, $lastname, $email, $mobilePrefix, $mobile, $function,
		$titlePrefix = '', $titleSuffix = '', $phonePrefix = '', $phone = '', $fax = '')
	{
		$body = $this->post('/api/createnewuser', [
			'OrganizationNumber' => $organizationNumber,
			'DegreeBefore' => $titlePrefix,
			'DegreeAfter' => $titleSuffix,
			'Name' => $name,
			'Surname' => $lastname,
			'Phone' => FormatHelper::implodePhone($phonePrefix, $phone),
			'Mobile' => FormatHelper::implodePhone($mobilePrefix, $mobile),
			'Fax' => $fax,
			'Role' => 'Běžný uživatel',
			'Function' => $function,
			'Email' => $email
		]);

		$this->emailService->sendConfirmRegistrationEmail($email, $body->getHash());

		return $body;
	}

	/**
	 * Aktivace účtu na základě DoubleOptionHash a hesla
	 *
	 * @param string $hash
	 * @param string $password
	 * @throws InvalidArgumentException
	 */
	public function activateAccountFromHash($hash, $password)
	{
		$hash = Tools\HashHelper::decodeActivationHash($hash);
		if (empty($hash) || count($hash) !== 2) {
			throw new InvalidArgumentException('Hash is not correct');
		}

		$this->changePassword($hash[1], $password);
		$this->hashVerification($hash[0]);
	}


	/**
	 * @param int $invoiceOrganizationNumber
	 * @return array
	 */
	public function getDeliveryAddresses($invoiceOrganizationNumber)
	{
		$key = $this->generateCacheKey(__FUNCTION__, func_get_args());

		$body = $this->cache->load($key);
		if ($body === null) {
			$body = $this->post('/api/getdeliveryaddresses', [
				'InvoiceOrganizationNumber' => $invoiceOrganizationNumber,
			])->getDecodedBody();

			$this->cache->save($key, $body, [
				Cache::EXPIRE => self::CACHE_EXPIRE,
				Cache::TAGS => [self::CACHE_TAG_GET_DELIVERY_ADDRESSES],
			]);
		}

		return $body;
	}


	/**
	 * @return array
	 */
	public function getNewRegistrations()
	{
		return $this->post('/api/getnewregistrations')->getDecodedBody();
	}


	/**
	 * 
	 * @param array $ids Id vztahů
	 */
	public function confirmNewRegistrations($ids)
	{
		$this->post('/api/confirmnewregistrations', ['IDs' => $ids]);
	}


	/**
	 * Vrátí výchozí doručovací adresu
	 *
	 * @param int $id Id vztahu
	 */
	public function getRelationDeliveryOrganization($id)
	{
		$key = $this->generateCacheKey(__FUNCTION__, func_get_args());

		$body = $this->cache->load($key);
		if ($body === null) {
			$body = $this->post('/api/getrelationdeliveryorganization', [
				'ID' => $id,
			])->getDecodedBody();

			$this->cache->save($key, $body, [
				Cache::EXPIRE => self::CACHE_EXPIRE,
			]);
		}

		return $body;
	}


	/**
	 * @param string $email
	 * @return array
	 */
	public function forgotPassword($email)
	{
		return $this->post('/api/forgotpassword', [
			'Email' => $email
		])->getDecodedBody();
	}


	public function updateConnectionForRelation($id, $connection, $type)
	{
		return $this->post('/api/updateconnectionforrelation', [
			'ID' => $id,
			'Connection' => $connection,
			'Type' => $type
		])->getDecodedBody();
	}


	public function updateContactPersonByUser($email, $name, $lastname, $mobilePrefix, $mobile, $titlePrefix = '',
		$titleSuffix = '', $phonePrefix = '', $phone = '', $fax = '')
	{
		return $this->post('/api/updatecontactpersonbyuser', [
			'Email' => $email,
			'DegreeBefore' => $titlePrefix,
			'DegreeAfter' => $titleSuffix,
			'Name' => $name,
			'Surname' => $lastname,
			'Phone' => FormatHelper::implodePhone($phonePrefix, $phone),
			'Mobile' => FormatHelper::implodePhone($mobilePrefix, $mobile),
			'Fax' => $fax,
		])->getDecodedBody();
	}


	public function updateContactPersonByAdmin($contactId, $email, $name, $lastname, $mobilePrefix, $mobile, $titlePrefix = '',
		$titleSuffix = '', $phonePrefix = '', $phone = '', $fax = '', $role = null, $function = '')
	{
		return $this->post('/api/updatecontactpersonbyadmin', [
			'ID' => $contactId,
			'Email' => $email,
			'DegreeBefore' => $titlePrefix,
			'DegreeAfter' => $titleSuffix,
			'Name' => $name,
			'Surname' => $lastname,
			'Phone' => FormatHelper::implodePhone($phonePrefix, $phone),
			'Mobile' => FormatHelper::implodePhone($mobilePrefix, $mobile),
			'Fax' => $fax,
			'Role' => empty($role) ? 'Běžný uživatel' : $role,
			'Function' => $function
		])->getDecodedBody();
	}


	public function cleanByTag($tag)
	{
		$this->cache->clean([Cache::TAGS => $tag]);
	}

}
