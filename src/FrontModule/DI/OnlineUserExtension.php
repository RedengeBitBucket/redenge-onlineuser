<?php

namespace Redenge\OnlineUser\FrontModule\DI;

use Nette\Application\IPresenterFactory;
use Nette\DI\CompilerExtension;
use Nette\DI\Statement;
use Nette\Utils\Validators;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKey;
use Redenge\OnlineUser\FrontModule\Provider;
use Redenge\OnlineUser\FrontModule\ProviderContainer;
use Redenge\OnlineUser\FrontModule\Settings;


class OnlineUserExtension extends CompilerExtension
{

		/**
	 * @var array
	 */
	public $defaults = [
		'http_client_handler' => null,
		'functions' => [],
	];


	/**
	 * return void
	 */
	public function loadConfiguration()
	{
		$config = $this->getConfig();

		$defaults = array_diff_key($this->defaults, $config);
		foreach ($defaults as $key => $val) {
			$config[$key] = $this->defaults[$key];
		}

		Validators::assertField($config, 'http_client_handler');
		Validators::assertField($config, 'functions');

		$builder = $this->getContainerBuilder();

		$providers = [];
		foreach ($config['settings'] as $settings) {
			$providers[] = new Statement(Provider::class, [
				'environmentKey' => (!($environment = $settings['environment']) instanceof Statement) ? 
					new Statement(EnvironmentKey::class, [$environment]) : $environment,
				'appUrl' => $settings['app_url'],
				'appUsername' => $settings['app_username'],
				'appPassword' => $settings['app_password']
			]);

		}

		$functions = [];
		foreach ($config['functions'] as $function) {
			$functions[$function['language']] = $function['functions'];
		}

		$builder->addDefinition($this->prefix('settings'))
			->setClass(Settings::class, [
				'httpClientHandler' => $config['http_client_handler'],
				'functions' => $functions,
		]);

		$builder->addDefinition($this->prefix('providerContainer'))
			->setClass(ProviderContainer::class)
			->setArguments([
				'providers' => $providers,
			]);

		$builder->addDefinition($this->prefix('appFactory'))
			->setClass(AppFactory::class, [$this->prefix('@settings'), $this->prefix('@providerContainer')]);
	}


	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();
		$builder->getDefinition($builder->getByType(IPresenterFactory::class))->addSetup(
			'setMapping',
			[['OnlineUser' => 'Redenge\OnlineUser\*Module\*Presenter']]
		);

		$this->compiler->loadDefinitions(
			$builder,
			$this->loadFromFile(__DIR__ . '/config.neon')['services']
		);

	}

}
