<?php

namespace Redenge\OnlineUser\FrontModule\Tools;

use DOMDocument;


class Ares
{

	public static function getData($ic)
	{
		if (!self::validateIC($ic)) {
			return null;
		}

		$doc = new DOMDocument();
		$doc->load(sprintf('http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=%s', rawurlencode($ic)));
		$pole = [];

		foreach ($doc->getElementsByTagnameNS('http://wwwinfo.mfcr.cz/ares/xml_doc/schemas/ares/ares_datatypes/v_1.0.3', "VBAS") as $vbas) {
			foreach ($vbas->getElementsByTagnameNS('http://wwwinfo.mfcr.cz/ares/xml_doc/schemas/ares/ares_datatypes/v_1.0.3', "*") as $item) {
				switch ($item->localName) {
					case 'DIC':
						$pole['dic'] = $item->nodeValue;
						break;
					case 'OF':
						$pole['companyName'] = $item->nodeValue;
						break;
					case 'PSC':
						$pole['zip'] = $item->nodeValue;
						break;
					case 'NU':
						$pole['street'] = $item->nodeValue;
						break;
					case 'N':
						$pole['city'] = $item->nodeValue;
						break;
					case 'NCO':
						$pole['nco'] = $item->nodeValue;
						break;
					case 'NFU':
						$pole['nfu'] = $item->nodeValue;
						break;
					case 'CD':
						$pole['streetNumber'] = $item->nodeValue;
						break;
					case 'CO':
						$pole['orientationNumber'] = $item->nodeValue !== '' ? $item->nodeValue : '';
						break;
				}
			}
		}

		if (!empty($pole)) {
			$pole['ic'] = $ic;
		}
		if (isset($pole['nco']) && $pole['nco'] !== '' && isset($pole['city']) && $pole['city'] !== '') {
			$pole['city'] = $pole['city'] . '-' . $pole['nco'];
		}

		return $pole;
	}


	/**
	 * @param string $ic
	 * @return bool
	 */
	public static function icExists($ic)
	{
		$data = self::getData($ic);

		return isset($data['ic']);
	}


	private static function validateIC($ic)
	{
		if (!preg_match('/^\d{8}$/', $ic))
			return false;

		$a = 0;

		for ($i = 0; $i < 7; $i++) {
			$a += $ic[$i] * (8 - $i);
		}

		$a = $a % 11;

		if ($a === 0)
			$c = 1;
		elseif ($a === 10)
			$c = 1;
		elseif ($a === 1)
			$c = 0;
		else
			$c = 11 - $a;

		return intval($ic[7]) === $c ? true : false;
	}

}
