<?php

namespace Redenge\OnlineUser\FrontModule\Tools;

use InvalidArgumentException;
use Nette\Forms\Controls\TextInput;
use Redenge\OnlineUser\FrontModule\App;


/**
 * Description of ValidationHelper
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class ValidationHelper
{

	public static function validateIcNotExists(TextInput $input, array $arg)
	{
		if (empty($input->value) || !isset($arg['api'])) {
			throw new InvalidArgumentException('Ic is empty');
		}

		/* @var $apiUser App */
		$apiUser = $arg['api'];
		return !$apiUser->existsIc($input->value);
	}


	public static function validateIcInAres(TextInput $input)
	{
		if (empty($input->value)) {
			throw new InvalidArgumentException('IC is empty');
		}

		return Ares::icExists($input->value);
	}


	public static function validateIcAresAndHeO(TextInput $input, array $arg)
	{
		if (empty($input->value) || !isset($arg['api'])) {
			throw new InvalidArgumentException('Ic is empty');
		}

		/* @var $apiUser App */
		$apiUser = $arg['api'];
		$ok = !$apiUser->existsIc($input->value);
		if ($ok) {
			$ok = Ares::icExists($input->value);
		}

		return $ok;
	}


	public static function validateEmailNotExists(TextInput $input, array $arg)
	{
		if (empty($input->value) || !isset($arg['api'])) {
			throw new InvalidArgumentException('Email is empty');
		}

		/* @var $apiUser App */
		$apiUser = $arg['api'];
		return !$apiUser->emailExists($input->value);
	}

}
