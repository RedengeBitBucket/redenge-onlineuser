<?php

namespace Redenge\OnlineUser\FrontModule\Tools;

/**
 * Description of HashHelper
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class HashHelper
{

	const SECRET_KEY = 'dsfkjg<456?pdl';
	const SECRET_IV = '123gdfgj<*&jj';
	const HASH_SEPARATOR = '|';

	/**
	 * @param string $string
	 * @return string
	 */
	public static function encode($string)
	{
		$encrypt_method = "AES-256-CBC";
		$key = hash('sha256', self::SECRET_KEY);
		$iv = substr(hash('sha256', self::SECRET_IV), 0, 16);

		return base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
	}


	/**
	 * @param string $string
	 * @return string
	 */
	public static function decode($string)
	{
		$encrypt_method = "AES-256-CBC";
		$key = hash('sha256', self::SECRET_KEY);
		$iv = substr(hash('sha256', self::SECRET_IV), 0, 16);

		return openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	}


	public static function generateActivationHash($hash, $email)
	{
		return self::encode($hash . self::HASH_SEPARATOR . $email);
	}


	/**
	 * @param string $hash
	 * @return array
	 */
	public static function decodeActivationHash($hash)
	{
		return explode(self::HASH_SEPARATOR, self::decode($hash));
	}

}
