<?php

namespace Redenge\OnlineUser\FrontModule\Tools;

use Nette\Utils\Strings;


/**
 * Description of FormatHelper
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class FormatHelper
{

	public static function formatZip($zip)
	{
		if (preg_match('~\d{3} \d{2}~', $zip)) {
			return $zip;
		} else {
			return Strings::substring($zip, 0, 3) . ' ' . Strings::substring($zip, 3, 2);
		}
	}


	public static function splitStreet($street, $streetNumber, $orientationNumber)
	{
		return $street
			. (!empty($streetNumber) ? ' ' . $streetNumber : '')
			. (!empty($orientationNumber) ? '/' . $orientationNumber : '');
	}


	public static function implodePhone($prefix, $phone)
	{
		return empty($phone) ? '' : $prefix . ' ' . trim($phone);
	}


	public static function implodeFullName($prefix, $name, $lastname, $suffix)
	{
		return (!empty($prefix) ? $prefix . ' ' : '')
			. $name . ' ' . $lastname
			. (!empty($suffix) ? ' ' . $suffix : '');
	}


	/**
	 * Rozdělení tel. cisla na předčíslí a zbytek
	 *
	 * @param string $phone
	 * @return array Array(0=>prefix, 1=>phone)
	 */
	public static function splitPhone($phone)
	{
		preg_match('/(\+\d{3})([\d\s]+)/', $phone, $matches);
		if (count($matches) === 3) {
			return [$matches[1], trim($matches[2])];
		} else {
			return [null, ''];
		}
	}
		
}
