<?php

namespace Redenge\OnlineUser\FrontModule;

use Redenge\OnlineUser\FrontModule\Http\RequestBodyUrlEncoded;
use Redenge\OnlineUser\FrontModule\Exceptions\ApiRedengeException;
use Redenge\OnlineUser\FrontModule\Tools\UrlHelper;

/**
 * Description of Request
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Request
{

	/**
	 * @var App entity.
	 */
	protected $app;

	/**
	 * @var string The HTTP method for this request.
	 */
	protected $method;

	/**
	 * @var string The endpoint for this request.
	 */
	protected $endpoint;

	/**
	 * @var array The headers to send with this request.
	 */
	protected $headers = [];

	/**
	 * @var array The parameters to send with this request.
	 */
	protected $params = [];


	/**
	 * Creates a new Request entity.
	 *
	 * @param App|null        $app
	 * @param string|null             $method
	 * @param string|null             $endpoint
	 * @param array|null              $params
	 */
	public function __construct(App $app = null, $method = null, $endpoint = null, array $params = [])
	{
		$this->setApp($app);
		$this->setMethod($method);
		$this->setEndpoint($endpoint);
		$this->setParams($params);
	}


	public function getApp()
	{
		return $this->app;
	}


	public function getMethod()
	{
		return $this->method;
	}


	public function getEndpoint()
	{
		return $this->endpoint;
	}


	public function getHeaders()
	{
		$headers = $this->getDefaultHeaders();

		return array_merge($this->headers, $headers);
	}


	public function getParams()
	{
		return $this->params;
	}


	 /**
     * Only return params on POST requests.
     *
     * @return array
     */
    public function getPostParams()
    {
        if ($this->getMethod() === 'POST') {
            return $this->getParams();
        }

        return [];
    }


	public function setApp(App $app)
	{
		$this->app = $app;
	}


	public function setMethod($method)
	{
		$this->method = strtoupper($method);
	}


	public function setEndpoint($endpoint)
	{
		$this->endpoint = $endpoint;
	}


	public function setHeaders(array $headers)
	{
		$this->headers = array_merge($this->headers, $headers);
	}


	public function setParams(array $params)
	{
		$this->params = array_merge($this->params, $params);
	}


	/**
	 * Generate and return the URL for this request.
	 *
	 * @return string
	 */
	public function getUrl()
	{
		$this->validateMethod();

		$baseUrl = $this->app->getProvider()->getAppUrl();
		$url = $baseUrl . UrlHelper::forceSlashPrefix($this->getEndpoint());

		if (!in_array($this->getMethod(), ['POST'])) {
			$params = $this->getParams();
			$url = UrlHelper::appendParamsToUrl($url, $params);
		}

		return $url;
	}


	/**
	 * Returns the body of the request as URL-encoded.
	 *
	 * @return RequestBodyUrlEncoded
	 */
	public function getUrlEncodedBody()
	{
		$params = $this->getPostParams();

		return new RequestBodyUrlEncoded($params);
	}


	/**
	 * Validate that the HTTP method is set.
	 *
	 * @throws ApiRedengeException
	 */
	private function validateMethod()
	{
		if (!$this->method) {
			throw new ApiRedengeException('HTTP method not specified.');
		}

		if (!in_array($this->method, ['GET', 'POST'])) {
			throw new ApiRedengeException('Invalid HTTP method specified.');
		}
	}


	/**
	 * Return the default headers that every request should use.
	 *
	 * @return array
	 */
	private function getDefaultHeaders()
	{
		$credentials = base64_encode(sprintf('%s:%s', $this->app->getProvider()->getAppUsername(), $this->app->getProvider()->getAppPassword()));

		return [
			'Authorization' => 'Basic ' . $credentials,
		];
	}

}
