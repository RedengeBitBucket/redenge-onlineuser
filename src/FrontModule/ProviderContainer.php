<?php

namespace Redenge\OnlineUser\FrontModule;

use OutOfBoundsException;


/**
 * Description of ProviderContainer
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class ProviderContainer
{

	/**
	 * @var Provider[]
	 */
	private $providers = [];


	/**
	 * @param Provider[] $providers
	 */
	public function __construct(array $providers)
	{
		foreach ($providers as $provider) {
			$this->addProvider($provider);
		}
	}


	/**
	 * @param Provider $provider
	 *
	 * @return void
	 */
	private function addProvider(Provider $provider)
	{
		$this->providers[] = $provider;
	}


	/**
	 * @param EnvironmentKey $environmentKey
	 *
	 * @return Provider
	 * @throws OutOfBoundsException
	 */
	public function getProvider(EnvironmentKey $environmentKey)
	{
		$matched = array_filter($this->providers, function (Provider $provider) use ($environmentKey) {
			return $provider->matchKey($environmentKey);
		});
		if (count($matched)) {
			return $matched[array_keys($matched)[0]];
		}

		return new Provider(new EnvironmentKey('0|0'), []);
	}


	public function getProviders()
	{
		return $this->providers;
	}

}
