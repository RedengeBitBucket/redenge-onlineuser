<?php

namespace Redenge\OnlineUser\FrontModule;

use Nette\Caching\IStorage;
use Nette\Http\Session;
use Redenge\OnlineUser\FrontModule\Mail\IMail;


/**
 * Description of AppFactory
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class AppFactory
{

	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * @var ProviderContainer
	 */
	private $providerContainer;

	/**
     * @var Client The client service.
     */
	private $client;

	/**
	 * @var IStorage
	 */
	private $storage;

	/**
	 * @var Session
	 */
	private $session;

	/**
	 * @var IMail
	 */
	private $emailService;


	public function __construct(
		Settings $settings,
		ProviderContainer $providerContainer,
		Client $client,
		IStorage $storage,
		Session $session,
		IMail $emailService)
	{
		$this->settings = $settings;
		$this->providerContainer = $providerContainer;
		$this->client = $client;
		$this->storage = $storage;
		$this->session = $session;
		$this->emailService = $emailService;
	}


	public function create(EnvironmentKey $environmentKey)
	{
		return new App(
			$this->settings,
			$this->providerContainer->getProvider($environmentKey),
			$this->client,
			$this->storage,
			$this->session,
			$this->emailService
		);
	}

}
