<?php

namespace Redenge\OnlineUser\FrontModule\Components\Registration\Containers;

use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Application\UI\Presenter;
use Nette\Database\Context;
use Nette\Forms\Container;
use Nette\Forms\Form;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Entity\Repository\CountryRepository;


/**
 * Description of CompanyContainer
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CompanyContainer extends Container
{

	/**
	 * @var Environment
	 */
	protected $environment;

	/**
	 * @var Context
	 */
	protected $db;


	public function __construct(
		Control $control,
		Presenter $presenter,
		AppFactory $apiUserFactory,
		Environment $environment,
		Context $db,
		Translator $translator,
		CountryRepository $countryRepository)
	{
		parent::__construct();

		$this->environment = $environment;
		$this->db = $db;
		$apiUser = $apiUserFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));

		$this->addText('ic', 'ic')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->setAttribute('autocomplete', 'off')
			->getLabelPrototype()->appendAttribute('class', 'required');
		if (strtoupper($environment->countryIso) === 'SK') {
			$this['ic']
				->addRule(Form::PATTERN, '//onlineUser.validation.ic_not_valid', '\d{8}')
				->addRule('Redenge\OnlineUser\FrontModule\Tools\ValidationHelper::validateIcNotExists',
					'//onlineUser.validation.ic_exists', ['link' => $presenter->link(':OnlineUser:Front:Validation:validateIcNotExists'), 'api' => $apiUser]);
		} elseif (strtoupper($environment->countryIso) === 'CZ') {
			$this['ic']
				->addRule(Form::PATTERN, '//onlineUser.validation.only_number', '\d+')
				->addRule(Form::MIN_LENGTH, '//onlineUser.validation.min_length', 7)
				->addRule(Form::MAX_LENGTH, '//onlineUser.validation.max_length', 11)
				->addRule('Redenge\OnlineUser\FrontModule\Tools\ValidationHelper::validateIcAresAndHeO',
					'//onlineUser.validation.ic_exists', ['link' => $presenter->link(':OnlineUser:Front:Validation:validateIcAresAndHeO'), 'api' => $apiUser]);
					
		} else {
			$this['ic']
				->addRule(Form::PATTERN, '//onlineUser.validation.only_number', '\d+')
				->addRule(Form::MIN_LENGTH, '//onlineUser.validation.min_length', 7)
				->addRule(Form::MAX_LENGTH, '//onlineUser.validation.max_length', 11)
				->addRule('Redenge\OnlineUser\FrontModule\Tools\ValidationHelper::validateIcNotExists',
					'//onlineUser.validation.ic_exists', ['link' => $presenter->link(':OnlineUser:Front:Validation:validateIcNotExists'), 'api' => $apiUser]);
		}
			
		if (strtoupper($this->environment->countryIso) === 'CZ') {
			$this->addButton('ares', 'ares_button')
				->setAttribute('class', 'btn btn-blue no-live-validation text-right')
				->setAttribute('data-spinner-target', '.onlineUser-form')
				->setAttribute('data-ajax-pass', 'true')
				->setAttribute('data-loadAresData', $control->link('getAresData!'));
		}

		$this->addText('dic', 'dic')
			->setAttribute('class', 'form-control')
			->setAttribute('autocomplete', 'off')
			->addConditionOn($this['dic'], FORM::FILLED)
				->addRule(Form::PATTERN, '//onlineUser.validation.dic_not_valid', '^([a-zA-Z]{2})(\d+){8,12}');

		if (strtoupper($environment->countryIso) === 'SK') {
			$this->addText('icDph', 'icDph')
				->setAttribute('class', 'form-control')
				->setAttribute('autocomplete', 'off')
				->addConditionOn($this['icDph'], FORM::FILLED)
					->addRule(Form::PATTERN, '//onlineUser.validation.ic_dph_not_valid', '^\d{10}');
		}
		
		$this->addText('companyName', 'company_name')
			->setAttribute('class', 'form-control');

		$this->addText('street', 'street')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->setAttribute('autocomplete', 'off')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$this->addText('streetNumber', 'streetNumber')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->setAttribute('autocomplete', 'off')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$this->addText('orientationNumber', 'orientationNumber')
			->setAttribute('class', 'form-control')
			->setAttribute('autocomplete', 'off');

		$this->addText('city', 'city')
			->setAttribute('class', 'form-control')
			->setRequired('//This field is required.')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$this->addText('zip', 'zip')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->addRule(Form::PATTERN, '//onlineUser.validation.zip_not_valid', '^([\d]{3})( )?([\d]{2})')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$this->addSelect('countryIso', $translator->translate('onlineUser.registration.country'), $countryRepository->getAssignedCountries($this->environment->profileCode))
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->setTranslator(NULL)
			->setValue($this->environment->countryIso);
	}

}
