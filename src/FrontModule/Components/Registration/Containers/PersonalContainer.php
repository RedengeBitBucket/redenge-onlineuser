<?php

namespace Redenge\OnlineUser\FrontModule\Components\Registration\Containers;

use Kdyby\Translation\Translator;
use Nette\Application\UI\Presenter;
use Nette\Forms\Container;
use Nette\Forms\Form;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Entity\Repository\CountryRepository;


/**
 * Description of PersonalContainer
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class PersonalContainer extends Container
{

	public function __construct(Presenter $presenter, AppFactory $apiUserFactory, Environment $environment, Translator $translator, CountryRepository $countryRepository)
	{
		parent::__construct();

		$countryPrefixes = $countryRepository->getMobilePrefixes();
		$mobilePrefixes = $countryRepository->formatMobilePrefixes($countryPrefixes);
		$apiUser = $apiUserFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));

		$this->addText('email', 'email')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->addRule(Form::EMAIL, '//Please enter a valid email address.')
			->addRule('Redenge\OnlineUser\FrontModule\Tools\ValidationHelper::validateEmailNotExists',
				'//onlineUser.validation.email_exists', ['link' => $presenter->link(':OnlineUser:Front:Validation:validateEmailNotExists'), 'api' => $apiUser])
			->getLabelPrototype()->appendAttribute('class', 'required');

		$this->addText('titlePrefix', 'title_prefix')
			->setAttribute('class', 'form-control');

		$this->addText('name', 'name')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$this->addText('lastname', 'lastname')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$this->addText('titleSuffix', 'title_suffix')
			->setAttribute('class', 'form-control');

		$this->addSelect('mobilePrefix', NULL, $mobilePrefixes)
			->setAttribute('class', 'form-control')
			->setTranslator(NULL);

		$this->addText('mobile', 'mobile')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->addRule(Form::PATTERN, '//onlineUser.validation.phoneFormat', '[\d ]+')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$this->addSelect('phonePrefix', NULL, $mobilePrefixes)
			->setAttribute('class', 'form-control')
			->setTranslator(NULL);

		$this->addText('phone', 'phone')
			->setAttribute('class', 'form-control')
			->addConditionOn($this['phone'], Form::FILLED)
				->addRule(Form::PATTERN, '//onlineUser.validation.phoneFormat', '[\d ]+');

		if (isset($countryPrefixes[strtoupper($environment->countryIso)])) {
			$this['mobilePrefix']->setValue($countryPrefixes[strtoupper($environment->countryIso)]);
			$this['phonePrefix']->setValue($countryPrefixes[strtoupper($environment->countryIso)]);
		}
	}

}
