<?php

namespace Redenge\OnlineUser\FrontModule\Components\Registration\Containers;

use Nette\Application\UI\Control;
use Nette\Application\UI\Presenter;


/**
 * Description of ICompanyContainer
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICompanyContainer
{

	/**
	 * @return CompanyContainer
	 */
	function create(Control $control, Presenter $presenter);

}
