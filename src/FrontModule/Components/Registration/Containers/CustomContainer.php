<?php

namespace Redenge\OnlineUser\FrontModule\Components\Registration\Containers;

use Kdyby\Translation\Translator;
use Nette\Forms\Container;


/**
 * Description of CustomContainer
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CustomContainer extends Container
{

	public function __construct(Translator $translator)
	{
		parent::__construct();

		$this->addHtml('customText')
			->setAttribute('class', 'form-control no-live-validation')
			->setHtml($translator->translate('onlineUser.registration.customText'));
	}

}
