<?php

namespace Redenge\OnlineUser\FrontModule\Components\Registration\Containers;

/**
 * Description of ICustomContainer
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICustomContainer
{

	/**
	 * @return CustomContainer
	 */
	function create();

}
