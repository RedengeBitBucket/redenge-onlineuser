<?php

namespace Redenge\OnlineUser\FrontModule\Components\Registration\Containers;

use Nette\Application\UI\Presenter;


/**
 * Description of IPersonalContainer
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IPersonalContainer
{

	/**
	 * @return PersonalContainer
	 */
	function create(Presenter $presenter);

}
