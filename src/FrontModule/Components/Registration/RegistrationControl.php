<?php

namespace Redenge\OnlineUser\FrontModule\Components\Registration;

use InvalidArgumentException;
use Kdyby\Events\EventArgsList;
use Kdyby\Events\EventManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\TextInput;
use Nette\Localization\ITranslator;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Components\Registration\Containers\ICompanyContainer;
use Redenge\OnlineUser\FrontModule\Components\Registration\Containers\IPersonalContainer;
use Redenge\OnlineUser\FrontModule\Components\Registration\Containers\ICustomContainer;
use Redenge\OnlineUser\FrontModule\Exceptions\ResponseException;
use Redenge\OnlineUser\FrontModule\Exceptions\SuccessException;
use Redenge\OnlineUser\FrontModule\Tools\Ares;
use Redenge\OnlineUser\FrontModule\Tools\FormatHelper;
use Redenge\OnlineUser\FrontModule\Tools\ValidationHelper;


/**
 * Description of RegistrationControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class RegistrationControl extends Control
{

	/**
	 * @var ICompanyContainer
	 */
	private $companyContainer;

	/**
	 * @var IPersonalContainer
	 */
	private $personalContainer;

	/**
	 * @var CustomContainer
	 */
	private $customContainer;

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var App
	 */
	private $apiUser;

	/**
	 * @var EventManager
	 */
	private $evm;


	public function __construct(
		ICompanyContainer $companyContainer,
		IPersonalContainer $personalContainer,
		ICustomContainer $customContainer,
		ITranslator $translator,
		AppFactory $apiUserFactory,
		Environment $environment,
		EventManager $evm)
	{
		$this->companyContainer = $companyContainer;
		$this->personalContainer = $personalContainer;
		$this->customContainer = $customContainer;
		$this->translator = $translator;
		$this->evm = $evm;
		$this->apiUser = $apiUserFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
	}


	/**
	 * @return Form
	 */
	public function createComponentForm()
	{
		$form = new Form;
		$form->setTranslator($this->translator->domain('onlineUser.registration'));

		$form['personalContainer'] = $this->personalContainer->create($this->presenter);
		$form['companyContainer'] = $this->companyContainer->create($this, $this->presenter);
		$form['customContainer'] = $this->customContainer->create();

		$form->addHtml('terms', 'terms')
			->setAttribute('class', 'terms-conditions');

		$form->addCheckbox('agreement', 'agreement')
			->setRequired('//This field is required.');

		$form->addSubmit('send', 'register')
			->setAttribute('class', 'ajax btn btn-default mr-2')
			->setAttribute('data-spinner-class', 'ajax-spinner-inline')
			->setAttribute('data-spinner-target', '.onlineUser-form .send-button')
			->setAttribute('data-spinner-place', 'append')
			->setAttribute('data-scrollTo', '.onlineUser-form');

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['label']['container'] = 'div class="col-sm-4"';
		$renderer->wrappers['pair']['container'] = 'div class="form-group row align-items-center"';
		$renderer->wrappers['control']['container'] = 'div class="col-sm-8"';

		$this->evm->dispatchEvent('Redenge\OnlineUser\FrontModule\Components\Registration\RegistrationControl::onTermsSet', new EventArgsList(array($form)));

		$form->onSuccess[] = [$this, 'onSuccessForm'];
		$form->onError[] = [$this, 'onErrorForm'];

		return $form;
	}


	public function onSuccessForm(Form $form)
	{
		$personalContainer = $form->values->personalContainer;
		$companyContainer = $form->values->companyContainer;

		try {
			$this->apiUser->newRegistration(
				$personalContainer->email,
				$personalContainer->name,
				$personalContainer->lastname,
				FormatHelper::implodePhone($personalContainer->mobilePrefix, $personalContainer->mobile),
				$companyContainer->street,
				$companyContainer->streetNumber,
				$companyContainer->city,
				$companyContainer->zip,
				$companyContainer->countryIso,
				$personalContainer->titlePrefix,
				$personalContainer->titleSuffix,
				FormatHelper::implodePhone($personalContainer->phonePrefix, $personalContainer->phone),
				$companyContainer->ic,
				$companyContainer->dic,
				isset($companyContainer->icDph) ? $companyContainer->icDph : '',
				$companyContainer->companyName,
				$companyContainer->orientationNumber
			);

			$this->flashMessage('onlineUser.registration.success', 'success');
			$this->redrawControl('formWrapper');

			$this->template->isSuccessForm = true;
		} catch (ResponseException $e) {
			$preException = $e->getPrevious();
            if ($preException instanceof SuccessException) {
                $this->flashMessage($preException->getMessage(), 'danger');
            } else {
				$this->flashMessage('onlineUser.registration.error', 'danger');
			}
			$this->redrawControl('formWrapper');
		}
	}


	public function onErrorForm(Form $form)
	{
		$this->flashMessage('onlineUser.validation.form_not_valid', 'danger');
		$this->redrawControl('formWrapper');
	}


	public function handleGetAresData()
	{
		$form = $this->createComponentForm();
		$personalContainer = $this->presenter->getParameter('personalContainer', []);
		$companyContainer = $this->presenter->getParameter('companyContainer', []);
		$ic = isset($companyContainer['ic']) ? $companyContainer['ic'] : null;
		if (empty($ic)) {
			return;
		}
		$icNotExists = ValidationHelper::validateIcNotExists((new TextInput)->setValue($ic), ['api' => $this->apiUser]);
		if ($icNotExists) {
			$aresData = Ares::getData($ic);
			if (empty($aresData)) {
				$this->presenter->terminate();
			}
			$companyContainer = $aresData;
			$form['personalContainer']->setDefaults($personalContainer);
			$form['companyContainer']->setDefaults($companyContainer);

			$this->addComponent($form, 'form');

			$this->redrawControl('formWrapper');
			
		} else {
			$this->presenter->payload->status = 'error';
			$this->presenter->payload->message = $this->translator->translate('onlineUser.validation.ic_exists');
			$this->presenter->sendPayload();
		}
	}


	public function render()
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/registrationControl.latte');

		$template->render();
	}

}
