<?php

namespace Redenge\OnlineUser\FrontModule\Components\Registration;

/**
 * Description of IRegistrationControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IRegistrationControl
{

	/**
	 * @return RegistrationControl
	 */
	function create();

}
