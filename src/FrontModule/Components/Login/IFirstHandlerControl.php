<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;


/**
 * Description of IFirstHandlerControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IFirstHandlerControl
{

	/**
	 * @return FirstHandlerControl
	 */
	function create();

}
