<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;

/**
 * Description of ISignInSuperAdminControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ISignInSuperAdminControl
{

	/**
	 * @return SignInSuperAdminControl
	 */
	function create();

}
