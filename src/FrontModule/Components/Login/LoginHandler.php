<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;

use Nette\Application\UI\Control;
use Redenge\OnlineUser\FrontModule\Response;


/**
 * Description of LoginHandler
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
abstract class LoginHandler extends Control
{

	/**
	 * @var Control
	 */
	private $successor = null;

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * Sets a successor handler.
	 *
	 * @param Control $handler
	 */
	public function setSuccessor(Control $handler)
	{
		if ($this->successor === null) {
			$this->successor = $handler;
		} else {
			$this->successor->setSuccessor($handler);
		}
	}


	/**
	 * Handles the request or redirect the request to the successor, if the process response is null.
	 *
	 * @param Response $response
	 * @return Control
	 */
	final public function handle($response)
	{
		$this->response = $response;
		$return = $this->getCurrentComponent();
		if (($return === null) && ($this->successor !== null)) {
			$return = $this->successor->handle($response);
		}

		return $return;
	}


	/**
	 * Processes the request.
	 * This is the only method a child can implements,
	 * with the constraint to return null to dispatch the request to next successor.
	 *
	 * @return null|Control
	 */
	abstract protected function getCurrentComponent();

	/**
	 * @return void
	 */
	abstract public function process();

}
