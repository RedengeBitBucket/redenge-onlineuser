<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;

use InvalidArgumentException;
use Kdyby\Translation\Translator;
use NasExt\Forms\DependentData;
use Nette\Application\UI\Form;
use Nette\Http\Session;
use Nette\Security\User;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;


/**
 * Pro zadané přihlašovací údaje existuje více fakturačních adres (FA) a jedna kontaktní adresa (KO) nebo
 * pro zadané přihlašovací údaje existuje více fakturačních adres (FA) a více kontaktních adres (KO)
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class FourthHandlerControl extends LoginHandler
{

	/**
	 * @var App
	 */
	protected $app;

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var \Nette\Http\SessionSection
	 */
	protected $sessionSection;

	/**
	 * @var Translator
	 */
	protected $translator;


	public function __construct(AppFactory $appFactory, Environment $environment, User $user, Session $session, Translator $translator)
	{
		$this->app = $appFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->user = $user;
		$this->sessionSection = $session->getSection('signInValues');
		$this->translator = $translator;
	}


	protected function getCurrentComponent()
	{
		$relations = $this->response->getRelations();
		$ids = [];
		foreach ($relations as $relation) {
			if (!isset($relation['ID'])) {
				continue;
			}
			$ids[] = (int) $relation['ID'];
		}

		if (count($ids) === 0) {
			throw new InvalidArgumentException('Argument ,,ID" is missing');
		}

		$this->sessionSection->organizations = $this->app->getInvoiceOrganizations($ids);
		if (count($this->sessionSection->organizations) === 0) {
			throw new InvalidArgumentException('Organizations is missing');
		}

		return $this;
	}


	public function process()
	{
	}


	/**
	 * @return Form
	 */
	public function createComponentForm()
	{
		$organizations = $this->getOrganizations();

		$form = new Form;
		$form->setTranslator($this->translator->domain('onlineUser.login'));
		$form->addSelect('organizationId', $this->translator->translate('onlineUser.login.billing_organization'), $organizations)
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->setPrompt($this->translator->translate('onlineUser.login.choose_organization'))
			->setTranslator(NULL);

		$form->addDependentSelectBox('contactId', $this->translator->translate('onlineUser.login.contact_person'), $form['organizationId'])
			->setDependentCallback(function ($values) use ($form) {
				$data = new DependentData;
				$organizationId = isset($values['organizationId']) ? $values['organizationId'] : null;
				if ($organizationId === null) {
					return $data;
				}

				$contacts = $this->app->getContactPersonsForInvoiceOrganization($organizationId, $this->sessionSection->response->getEmail());
				$this->sessionSection->contacts = $contacts;
				$contacts = $this->getContacts();
				$data->setItems($contacts);
				if (count($contacts) === 1) {
					$form['contactId']->setDisabled()->setPrompt(false);
				} else {
					$data->setPrompt($this->translator->translate('onlineUser.login.choose_contact_person'));
				}

				return $data;
			})
			->setAttribute('class', 'form-control')
			->setRequired('//This field is required.')
			->setPrompt($this->translator->translate('onlineUser.login.choose_organization_first'))
			->setTranslator(NULL);

		$form->addSubmit('send', 'login')
			->setAttribute('class', 'ajax btn btn-default')
			->setAttribute('data', 'ajax-spinner');

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['label']['container'] = 'div class="col-sm-4"';
		$renderer->wrappers['pair']['container'] = 'div class="form-group row align-items-center"';
		$renderer->wrappers['control']['container'] = 'div class="col-sm-8"';

		$form->onSuccess[] = [$this, 'onFormSuccess'];

		return $form;
	}


	public function onFormSuccess(Form $form)
	{
		$values = $form->values;

		$organizationId = isset($values->organizationId) ? $values->organizationId : null;
		$contactId = isset($values->contactId) ? $values->contactId : null;
		if ($organizationId === null) {
			$form->addError('Nebyla vybrána fakturační organizace');
		} elseif ($organizationId !== null && $contactId !== null) {
			$contact = $this->app->getContactPerson($contactId);
			$this->user->login($contact);
		} else {
			$contacts = $this->sessionSection->contacts;
			if (count($contacts) === 0) {
				throw new InvalidArgumentException('Relation of contacts is missing');
			}
			if (count($contacts) === 1) {
				$contact = current($contacts);
				$contactId = (int) $contact['ID'];
				$contact = $this->app->getContactPerson($contactId);

				$this->user->login($contact);
			}
		}
	}


	public function render()
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/form.latte');

		$template->render();
	}


	/**
	 * @return array
	 */
	private function getOrganizations()
	{
		if (empty($this->sessionSection->organizations)) {
			return [];
		}
		$return = [];
		foreach ($this->sessionSection->organizations as $organization) {
			$return[$organization['OrganizationNumber']] = $organization['Name'];
		}

		return $return;
	}


	/**
	 * @return array
	 */
	private function getContacts()
	{
		if (empty($this->sessionSection->contacts)) {
			return [];
		}
		$return = [];
		foreach ($this->sessionSection->contacts as $contact) {
			$return[$contact['ID']] = $contact['Name'] . ' ' . $contact['Surname'];
		}

		return $return;
	}

}
