<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;


/**
 * Description of IFourthHandlerControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IFourthHandlerControl
{

	/**
	 * @return FourthHandlerControl
	 */
	function create();

}
