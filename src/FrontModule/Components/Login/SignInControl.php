<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;

use Exception;
use Kdyby\Translation\Translator;
use Nette\Application\AbortException;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Security\AuthenticationException;
use Nette\Security\User;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Exceptions\ResponseException;
use Redenge\OnlineUser\FrontModule\Exceptions\SuccessException;


/**
 * Description of SignInControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class SignInControl extends Control
{

	/**
	 * @var App
	 */
	protected $app;

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var SessionSection
	 */
	protected $sessionSection;

	/**
	 * @var LoginFactory
	 */
	protected $loginFactory;

	/**
	 * @var Translator
	 */
	protected $translator;


	/**
	 * @persistent
	 *
	 * Tento příznak nám označuje, jestli se zobrazí základní přihlašovací formulář a nebo formulář ,,po přihlášeni"
	 */
    public $loginInProcess = false;


	public function __construct(AppFactory $appFactory, Environment $environment, User $user, LoginFactory $loginFactory, Session $session, Translator $translator)
	{
		$this->app = $appFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->user = $user;
		$this->user->onLoggedIn[] = [$this, 'onLoggedIn'];
		$this->loginFactory = $loginFactory;
		$this->sessionSection = $session->getSection('signInValues');
		$this->translator = $translator;
	}


	/**
	 * Do session ukládáme odpoveď z API redenge. Pokud skončila platnost session, tak nastavíme příznak
	 * ,,loginInProcess" na false.
	 *
	 * @param array $params
	 */
	public function loadState(array $params)
	{
		if (isset($params['loginInProcess']) && empty($this->sessionSection->response)) {
			$params['loginInProcess'] = false;
		}

		parent::loadState($params);
	}


	/**
	 * Zpracování události přihlášení zakázníka
	 * Akce, které se vykonají při přihlášení zákazníka
	 */
	public function onLoggedIn()
	{
		$this->sessionSection->remove();
		$this->presenter->redirectUrl(App::REDIRECT_URL);
	}


	/**
	 * @return Form
	 */
	public function createComponentBaseForm()
	{
		$form = new Form;
		$form->setTranslator($this->translator->domain('onlineUser.login'));
		$form->addText('email', 'email')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->setAttribute('data-lfv-message-id', 'frm-email_message')
			->addRule(Form::EMAIL, '//Please enter a valid email address.');

		$form->addPassword('password', 'password')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->setAttribute('data-lfv-message-id', 'frm-password_message');

		$form->addSubmit('send', 'login')
			->setAttribute('class', 'ajax btn btn-default')
			->setAttribute('data-spinner-target', '#core-modal-content');

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['label']['container'] = 'div class="col-sm-4"';
		$renderer->wrappers['pair']['container'] = 'div class="form-group row align-items-center"';
		$renderer->wrappers['control']['container'] = 'div class="col-sm-8"';

		$form->onSuccess[] = [$this, 'baseFormSuccess'];

		return $form;
	}


	public function baseFormSuccess(Form $form)
	{
		$values = $form->values;

		try {
			$response = $this->app->login($values->email, $values->password);
			if ($response->isEmpty()) {
				throw new AuthenticationException('onlineUser.login.bad_login_information');
			}

			$response->setEmail($values->email);
			$this->sessionSection->response = $response;
			
			$this->loginInProcess = true;

			/**
			 * Po přihlášení je třeba hned šáhnout na komponentu (dříve, než to udělá render metoda)
			 * Nastával problém, že v momentě, kdy jsem chtěl zákazníka ihned přihlásit, tak se v ajax odpovědi
			 * neobjevila informace o přesměrování
			 */
			$this['loginForm'];
		} catch (AbortException $ex) {
			throw $ex;
		} catch (Exception $ex) {
			$this->flashMessage($ex->getMessage(), 'danger');		
		}
		
		$this->redrawControl();
	}


	public function createComponentLoginForm()
	{
		if (empty($this->sessionSection->response)) {
			$this->presenter->redirectUrl('/');
		}
		try {
			$control = $this->loginFactory->create($this->sessionSection->response);
			if ($control === null) {
				$control = $this->createComponentBaseForm();
				$this->addComponent($control, 'loginForm');
			} else {
				$this->addComponent($control, 'loginForm');
				$control->process();
			}
		} catch (AbortException $ex) {
			throw $ex;
		} catch (Exception $ex) {
			$this->flashMessage($ex->getMessage());
		}
	}


	public function createComponentForgotPass()
	{
		$form = new Form;
		$form->setTranslator($this->translator->domain('onlineUser.forgotPassword'))
			->getElementPrototype()->appendAttribute('class', 'onlineUser-form');

		$form->addText('email', 'enterYourEmailAddress')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->addRule(Form::EMAIL, '//Please enter a valid email address.')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addSubmit('send', 'send')
			->setAttribute('class', 'ajax btn btn-default')
			->setAttribute('data-spinner-target', '#core-modal-content');

		$form->addButton('back', 'backToLogin')
			->setAttribute('data-toggle', 'collapse')
			->setAttribute('aria-expanded', 'false')
			->setAttribute('aria-controls', 'signInForm')
			->setAttribute('data-target', '#signInForm')
			->setAttribute('class', 'btn btn-secondary');

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['label']['container'] = 'div class="col-sm-4"';
		$renderer->wrappers['pair']['container'] = 'div class="form-group row align-items-center"';
		$renderer->wrappers['control']['container'] = 'div class="col-sm-8"';

		$form->onSuccess[] = [$this, 'onSuccessForgotPass'];

		return $form;
	}


	public function onSuccessForgotPass(Form $form)
	{
		try {
			$response = $this->app->forgotPassword($form->values->email);

			$email = isset($response['Email']) ? $response['Email'] : null;
			$hash = isset($response['DoubleOptInHash']) ? $response['DoubleOptInHash'] : null;
			if (empty($email) || empty($hash)) {
				throw new Exception();
			}

			$this->app->getEmailService()->sendForgotPasswordEmail($email, $hash);
			$this->flashMessage('onlineUser.forgotPassword.onSuccess', 'success');
		} catch (ResponseException $e) {
			$preException = $e->getPrevious();
            if ($preException instanceof SuccessException) {
                $this->flashMessage($preException->getMessage(), 'danger');
            } else {
				$this->flashMessage('onlineUser.forgotPassword.onError', 'danger');
			}
		} catch (Exception $e) {
			$this->flashMessage('onlineUser.forgotPassword.onError', 'danger');
		}

		$this->template->forgotPassShow = true;
		$this->redrawControl('forgotPassWrapper');
	}


	public function render()
	{
		$template = $this->template;
		$template->setFile(__DIR__ . '/templates/signInControl.latte');

		$template->loginInProcess = $this->loginInProcess;
		$template->render();
	}

}
