<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;

use Exception;
use Kdyby\Translation\Translator;
use Nette\Application\AbortException;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Security\AuthenticationException;
use Nette\Security\User;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Exceptions\ResponseException;
use Redenge\OnlineUser\FrontModule\Exceptions\SuccessException;


/**
 * Description of SignInSuperAdminControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class SignInSuperAdminControl extends Control
{

	/**
	 * @var App
	 */
	protected $app;

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var SessionSection
	 */
	protected $sessionSection;

	/**
	 * @var LoginFactory
	 */
	protected $loginFactory;

	/**
	 * @var Translator
	 */
	protected $translator;

	/**
	 * @persistent
	 *
	 * Tento příznak nám označuje, jestli se zobrazí základní přihlašovací formulář a nebo formulář ,,po přihlášeni"
	 */
    public $loginInProcess = false;


	public function __construct(AppFactory $appFactory, Environment $environment, User $user, LoginFactory $loginFactory, Session $session, Translator $translator)
	{
		$this->app = $appFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->user = $user;
		$this->user->onLoggedIn[] = [$this, 'onLoggedIn'];
		$this->loginFactory = $loginFactory;
		$this->sessionSection = $session->getSection('signInValues');
		$this->translator = $translator;
	}


	/**
	 * Do session ukládáme odpoveď z API redenge. Pokud skončila platnost session, tak nastavíme příznak
	 * ,,loginInProcess" na false.
	 *
	 * @param array $params
	 */
	public function loadState(array $params)
	{
		if (isset($params['loginInProcess']) && empty($this->sessionSection->response)) {
			$params['loginInProcess'] = false;
		}

		parent::loadState($params);
	}


	/**
	 * Zpracování události přihlášení zakázníka
	 * Akce, které se vykonají při přihlášení zákazníka
	 */
	public function onLoggedIn()
	{
		$this->presenter->redirectUrl(App::REDIRECT_URL);
	}


	/**
	 * @return Form
	 */
	public function createComponentBaseForm()
	{
		$form = new Form;
		$form->setTranslator($this->translator->domain('onlineUser.login'));
		$form->addText('email', 'email')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->setAttribute('data-lfv-message-id', 'frm-email_message')
			->addRule(Form::EMAIL, '//Please enter a valid email address.');

		$form->addSubmit('send', 'login')
			->setAttribute('class', 'ajax btn btn-default')
			->setAttribute('data-spinner-target', '#core-modal-content');

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['label']['container'] = 'div class="col-sm-4"';
		$renderer->wrappers['pair']['container'] = 'div class="form-group row align-items-center"';
		$renderer->wrappers['control']['container'] = 'div class="col-sm-8"';

		$form->onSuccess[] = [$this, 'baseFormSuccess'];

		return $form;
	}


	public function baseFormSuccess(Form $form)
	{
		$values = $form->values;

		try {
			$response = $this->app->superAdminLogin($values->email);
			if ($response->isEmpty()) {
				throw new AuthenticationException('onlineUser.login.bad_login_information');
			}

			$response->setEmail($values->email);
			$this->sessionSection->response = $response;
			
			$this->loginInProcess = true;

			/**
			 * Po přihlášení je třeba hned šáhnout na komponentu (dříve, než to udělá render metoda)
			 * Nastával problém, že v momentě, kdy jsem chtěl zákazníka ihned přihlásit, tak se v ajax odpovědi
			 * neobjevila informace o přesměrování
			 */
			$this['loginForm'];
		} catch (AbortException $ex) {
			throw $ex;
		} catch (Exception $ex) {
			$this->flashMessage($ex->getMessage(), 'danger');		
		}
		
		$this->redrawControl();
	}


	public function createComponentLoginForm()
	{
		if (empty($this->sessionSection->response)) {
			$this->presenter->redirectUrl('/');
		}
		try {
			$control = $this->loginFactory->create($this->sessionSection->response);
			if ($control === null) {
				$control = $this->createComponentBaseForm();
				$this->addComponent($control, 'loginForm');
			} else {
				$this->addComponent($control, 'loginForm');
				$control->process();
			}
		} catch (AbortException $ex) {
			throw $ex;
		} catch (Exception $ex) {
			$this->flashMessage($ex->getMessage());
		}
	}


	public function render()
	{
		$template = $this->template;
		$template->setFile(__DIR__ . '/templates/signInSuperAdminControl.latte');

		$template->loginInProcess = $this->loginInProcess;
		$template->render();
	}
}
