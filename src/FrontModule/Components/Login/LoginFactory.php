<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;

use Nette\Security\AuthenticationException;
use Redenge\OnlineUser\FrontModule\Response;


/**
 * Description of LoginFactory
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class LoginFactory
{

	/**
	 * @var IFirstHandlerControl
	 */
	protected $firstHandlerFactory;

	/**
	 * @var ISecondHandlerControl
	 */
	protected $secondHandlerFactory;

	/**
	 * @var IThirdHandlerControl
	 */
	protected $thirdHandlerFactory;

	/**
	 * @var IFourthHandlerControl
	 */
	protected $fourthHandlerFactory;


	public function __construct(
		IFirstHandlerControl $firstHandlerFactory,
		ISecondHandlerControl $secondHandlerFactory,
		IThirdHandlerControl $thirdHandlerFactory,
		IFourthHandlerControl $fourthHandlerFactory
	) {
		$this->firstHandlerFactory = $firstHandlerFactory;
		$this->secondHandlerFactory = $secondHandlerFactory;
		$this->thirdHandlerFactory = $thirdHandlerFactory;
		$this->fourthHandlerFactory = $fourthHandlerFactory;
	}


	/**
	 * Vrátí komponentu na základě odpovědi z API při přihlášení
	 * Využit návrhový vzor Chain of Responsibility
	 *
	 * @param Response $response
	 * @return LoginHandler|null
	 * @throws AuthenticationException
	 */
	public function create(Response $response)
	{
		if ($response->isEmpty()) {
			throw new AuthenticationException('onlineUser.login.bad_login_information');
		}

		$firstHandler = $this->firstHandlerFactory->create();
		$secondHandler = $this->secondHandlerFactory->create();
		$thirdHandler = $this->thirdHandlerFactory->create();
		$fourthHandler = $this->fourthHandlerFactory->create();

		$firstHandler->setSuccessor($secondHandler);
		$firstHandler->setSuccessor($thirdHandler);
		$firstHandler->setSuccessor($fourthHandler);
		$control = $firstHandler->handle($response);

		return $control;
	}

}
