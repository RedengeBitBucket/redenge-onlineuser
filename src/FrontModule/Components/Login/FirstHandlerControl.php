<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;

use Nette\Caching\Cache;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Mail\IMail;


/**
 * Pro zadané přihlašovací údaje existuje vygenerovaný hash v poli ,,DoubleOptInHash"
 * Znamená to, že musí dojít k aktivaci účtu. Proto zasíláme email s linkem pro aktivaci
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class FirstHandlerControl extends LoginHandler
{

	/**
	 * @var App
	 */
	protected $app;

	/**
	 * @var IMail
	 */
	protected $emailService;


	public function __construct(AppFactory $appFactory, Environment $environment, IMail $emailService)
	{
		$this->app = $appFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->emailService = $emailService;
	}


	/**
	 * {@inheritdoc}
	 */
	protected function getCurrentComponent()
	{
		if (empty($this->response->getHash())) {
			return null;
		}

		return $this;
	}


	public function process()
	{
		$this->emailService->sendActivationMail($this->response->getEmail(), $this->response->getHash());
		$this->app->getCache()->clean([
			Cache::TAGS => App::CACHE_TAG_LOGIN
		]);
	}


	public function render()
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/firstHandlerControl.latte');

		$template->render();
	}

}
