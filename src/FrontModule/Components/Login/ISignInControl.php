<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;


/**
 * Description of ISignInControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ISignInControl
{

	/**
	 * @return SignInControl
	 */
	function create();

}
