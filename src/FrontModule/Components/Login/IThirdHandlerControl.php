<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;


/**
 * Description of IThirdHandlerControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IThirdHandlerControl
{

	/**
	 * @return ThirdHandlerControl
	 */
	function create();

}
