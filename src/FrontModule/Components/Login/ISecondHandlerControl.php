<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;


/**
 * Description of ISecondHandlerControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ISecondHandlerControl
{

	/**
	 * @return SecondHandlerControl
	 */
	function create();

}
