<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;

use Nette\Security\User;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;


/**
 * Pro zadané přihlašovací údaje existuje jedna fakturační adresa (FA) a jedná kontaktní adresa (KO)
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class SecondHandlerControl extends LoginHandler
{

	/**
	 * @var App
	 */
	protected $app;

	/**
	 * @var User
	 */
	protected $user;


	public function __construct(AppFactory $appFactory, Environment $environment, User $user)
	{
		$this->app = $appFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->user = $user;
	}


	protected function getCurrentComponent()
	{
		$relations = $this->response->getRelations();
		if (count($relations) === 1) {
			return $this;
		}

		return null;
	}


	public function process()
	{
		$relation = current($this->response->getRelations());
		$id = isset($relation['ID']) ? (int) $relation['ID'] : null;
		$role = isset($relation['Role']) ? $relation['Role'] : null;

		if ($id <= 0) {
			throw new InvalidArgumentException('Argument ,,ID" is missing');
		}

		$contact = $this->app->getContactPerson($id);

		$this->user->login($contact);
	}

	public function render()
	{
		
	}
}
