<?php

namespace Redenge\OnlineUser\FrontModule\Components\Login;

use InvalidArgumentException;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;
use Nette\Http\Session;
use Nette\Security\User;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;


/**
 * Pro zadané přihlašovací údaje existuje jedna fakturační adresa (FA), ale více kontaktních osob (KO)
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class ThirdHandlerControl extends LoginHandler
{

	/**
	 * @var App
	 */
	protected $app;

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var array
	 */
	private $contacts;

	/**
	 * @var Translator
	 */
	protected $translator;

	/**
	 * @var \Nette\Http\SessionSection
	 */
	protected $sessionSection;


	public function __construct(AppFactory $appFactory, Environment $environment, User $user, Translator $translator, Session $session)
	{
		$this->app = $appFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->user = $user;
		$this->translator = $translator;
		$this->sessionSection = $session->getSection('signInValues');
	}


	protected function getCurrentComponent()
	{
		$relations = $this->response->getRelations();
		$ids = [];
		foreach ($relations as $relation) {
			if (!isset($relation['ID'])) {
				continue;
			}
			$ids[] = (int) $relation['ID'];
		}

		if (count($ids) === 0) {
			throw new InvalidArgumentException('Argument ,,ID" is missing');
		}

		$organizations = $this->app->getInvoiceOrganizations($ids);
		if (count($organizations) === 0) {
			throw new InvalidArgumentException('Organizations is missing');
		}
		if (count($organizations) !== 1) {
			return null;
		}
		$organization = current($organizations);
		$id = isset($organization['OrganizationNumber']) ? (int) $organization['OrganizationNumber'] : null;
		if ($id <= 0) {
			throw new InvalidArgumentException('Argument ,,OrganizationNumber" is missing');
		}

		$this->contacts = $this->app->getContactPersonsForInvoiceOrganization($id, $this->sessionSection->response->getEmail());
		if (count($this->contacts) === 0) {
			throw new InvalidArgumentException('Relation of contacts is missing');
		}
		if (count($this->contacts) > 1) {
			return $this;
		}

		return null;
	}


	public function process()
	{
	}


	/**
	 * @return Form
	 */
	public function createComponentForm()
	{
		$contacts = $this->getContacts();

		$form = new Form;
		$form->setTranslator($this->translator->domain('onlineUser.login'));
		$form->addSelect('contactId', $this->translator->translate('onlineUser.login.contact_person'), $contacts)
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->setPrompt($this->translator->translate('onlineUser.login.choose_contact_person'))
			->setTranslator(NULL);

		$form->addSubmit('send', 'login')
			->setAttribute('class', 'ajax btn btn-default')
			->setAttribute('data', 'ajax-spinner');

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['label']['container'] = 'div class="col-sm-4"';
		$renderer->wrappers['pair']['container'] = 'div class="form-group row align-items-center"';
		$renderer->wrappers['control']['container'] = 'div class="col-sm-8"';

		$form->onSuccess[] = [$this, 'onFormSuccess'];

		return $form;
	}


	public function onFormSuccess(Form $form)
	{
		$values = $form->values;

		$id = isset($values->contactId) ? $values->contactId : null;
		if ($id === null) {
			$form->addError('Nebyla vybrána kontaktní osoba');
		} else {
			$contact = $this->app->getContactPerson($id);

			$this->user->login($contact);
		}
	}


	public function render()
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/form.latte');

		$template->render();
	}


	/**
	 * @return array
	 */
	private function getContacts()
	{
		$return = [];
		foreach ($this->contacts as $contact) {
			$return[$contact['ID']] = $contact['Name'] . ' ' . $contact['Surname'];
		}

		return $return;
	}

}
