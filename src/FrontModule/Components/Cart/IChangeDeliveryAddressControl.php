<?php

namespace Redenge\OnlineUser\FrontModule\Components\Cart;

/**
 * Description of IChangeDeliveryAddressControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IChangeDeliveryAddressControl
{

	/**
	 * @return ChangeDeliveryAddressControl
	 */
	function create();

}
