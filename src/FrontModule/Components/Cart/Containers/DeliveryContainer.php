<?php

namespace Redenge\OnlineUser\FrontModule\Components\Cart\Containers;

use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Forms\Container;
use Nette\Security\User;
use Redenge\OnlineUser\FrontModule\Entity\Repository\CountryRepository;
use Redenge\OnlineUser\FrontModule\Tools\FormatHelper;


/**
 * Description of DeliveryContainer
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class DeliveryContainer extends Container
{

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var Translator
	 */
	protected $translator;

	/**
	 * @var CountryRepository
	 */
	protected $countryRepository;


	public function __construct(Control $control, User $user, Translator $translator, CountryRepository $countryRepository)
	{
		$this->user = $user;
		$this->translator = $translator;
		$this->countryRepository = $countryRepository;

		$this->addText('companyName', 'companyName')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		$this->addText('streetSplit', 'street')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		$this->addText('city', 'city')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		$this->addText('zip', 'zip')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		$this->addText('country', 'country')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		$this->addButton('chooseAddress', 'chooseAnotherAddress')
			->setAttribute('class', 'btn btn-secondary')
			->setAttribute('data-toggle', 'modal')
			->setAttribute('data-target', '#addressList')
			->setAttribute('href', $control->link('showDeliveryAddressForm!'))
			->setAttribute('data-spinner-target', '#addressList');
		$this->addHidden('street');
		$this->addHidden('streetNumber');
		$this->addHidden('orientationNumber');
		$this->addHidden('countryId');

		$this->setDefaults($this->getDefaults());
	}


	public function setAddress(\Redenge\OnlineUser\FrontModule\Entity\Address $address)
	{
		$this->setDefaults([
			'companyName' => $address->name,
			'street' => $address->street,
			'streetNumber' => $address->houseNumber,
			'orientationNumber' => $address->orientationNumber,
			'streetSplit' => FormatHelper::splitStreet($address->street, $address->houseNumber, $address->orientationNumber),
			'city' => $address->city,
			'zip' => $address->zipPostalCode,
			'country' => $this->translator->translate('country.' . $address->country),
			'countryId' => $this->countryRepository->getCountryId($address->country)
		]);
	}


	/**
	 * @return array
	 */
	private function getDefaults()
	{
		return [
			'companyName' => $this->user->identity->deliveryAddress->name,
			'street' => $this->user->identity->deliveryAddress->street,
			'streetNumber' => $this->user->identity->deliveryAddress->houseNumber,
			'orientationNumber' => $this->user->identity->deliveryAddress->orientationNumber,
			'streetSplit' => FormatHelper::splitStreet($this->user->identity->deliveryAddress->street, $this->user->identity->deliveryAddress->houseNumber, $this->user->identity->deliveryAddress->orientationNumber),
			'city' => $this->user->identity->deliveryAddress->city,
			'zip' => $this->user->identity->deliveryAddress->zipPostalCode,
			'country' => $this->translator->translate('country.' . $this->user->identity->deliveryAddress->country),
			'countryId' => $this->countryRepository->getCountryId($this->user->identity->address->country)
		];
	}

}
