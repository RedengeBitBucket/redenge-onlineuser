<?php

namespace Redenge\OnlineUser\FrontModule\Components\Cart\Containers;

/**
 * Description of IBillingContainer
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IBillingContainer
{

	/**
	 * @return BillingContainer
	 */
	function create();

}
