<?php

namespace Redenge\OnlineUser\FrontModule\Components\Cart\Containers;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Security\User;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\Entity\Repository\CountryRepository;
use Redenge\OnlineUser\FrontModule\Tools\FormatHelper;


/**
 * Description of PersonalContainer
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class PersonalContainer extends Container
{

	/**
	 * @var User
	 */
	protected $user;


	public function __construct(Control $control, User $user, Environment $environment, CountryRepository $countryRepository)
	{
		$this->user = $user;

		$countryPrefixes = $countryRepository->getMobilePrefixes();
		$mobilePrefixes = $countryRepository->formatMobilePrefixes($countryPrefixes);

		$this->addText('email', 'email')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');

		$this->addText('name', 'contact_person')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');

		$this->addSelect('mobilePrefix', NULL, $mobilePrefixes)
			->setAttribute('class', 'form-control-plaintext')
			->setTranslator(NULL);

		$this->addText('mobile', 'mobile')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly')
			->setAttribute('autocomplete', 'off')
			->setAttribute('inlineEdit', $control->link('changePhone!', ['type' => App::CONNECTION_TYPE_MOBILE]))
			->setAttribute('placeholder', '//onlineUser.validation.phoneFormat')
			->addConditionOn($this['mobile'], Form::FILLED)
				->addRule(Form::PATTERN, '//onlineUser.validation.phoneFormat', '[\d ]+');

		$this->addSelect('phonePrefix', NULL, $mobilePrefixes)
			->setAttribute('class', 'form-control-plaintext')
			->setTranslator(NULL);

		$this->addText('phone', 'phone')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly')
			->setAttribute('autocomplete', 'off')
			->setAttribute('inlineEdit', $control->link('changePhone!', ['type' => App::CONNECTION_TYPE_PHONE]))
			->setAttribute('placeholder', '//onlineUser.validation.phoneFormat')
			->addConditionOn($this['phone'], Form::FILLED)
				->addRule(Form::PATTERN, '//onlineUser.validation.phoneFormat', '[\d ]+');

		$this->addHidden('firstname');
		$this->addHidden('lastname');

		if (isset($countryPrefixes[strtoupper($environment->countryIso)])) {
			$this['mobilePrefix']->setValue($countryPrefixes[strtoupper($environment->countryIso)]);
			$this['phonePrefix']->setValue($countryPrefixes[strtoupper($environment->countryIso)]);
		}

		list($mobilePrefix, $mobile) = FormatHelper::splitPhone($user->identity->mobile);
		list($phonePrefix, $phone) = FormatHelper::splitPhone($user->identity->phone);

		$this->setDefaults([
			'email' => $user->identity->email,
			'name' => $this->getContactPersonName(),
			'mobilePrefix' => $mobilePrefix,
			'mobile' => $mobile,
			'phonePrefix' => $phonePrefix,
			'phone' => $phone,
			'firstname' => $user->identity->name,
			'lastname' => $user->identity->surname,
		]);
	}


	private function getContactPersonName()
	{
		return (!empty($this->user->identity->degreeBefore) ? $this->user->identity->degreeBefore . ' ' : '')
			. $this->user->identity->name
			. ' ' . $this->user->identity->surname
			. (!empty($this->user->identity->degreeAfter) ? ' ' . $this->user->identity->degreeAfter : '');
	}
}
