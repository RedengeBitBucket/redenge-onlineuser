<?php

namespace Redenge\OnlineUser\FrontModule\Components\Cart\Containers;

use Kdyby\Translation\Translator;
use Nette\Forms\Container;
use Nette\Security\User;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\Entity\Repository\CountryRepository;
use Redenge\OnlineUser\FrontModule\Tools\FormatHelper;


/**
 * Description of BillingContainer
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class BillingContainer extends Container
{

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var Translator
	 */
	protected $translator;

	/**
	 * @var CountryRepository
	 */
	protected $countryRepository;


	public function __construct(
		User $user,
		Environment $environment,
		Translator $translator,
		CountryRepository $countryRepository)
	{
		$this->user = $user;
		$this->translator = $translator;
		$this->countryRepository = $countryRepository;

		$this->addText('ic', 'ic')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		$this->addText('dic', 'dic')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		if ($environment->countryIso === 'SK') {
			$this->addText('icDph', 'icDph')
				->setAttribute('class', 'form-control-plaintext w-100')
				->setAttribute('readonly');
		}
		$this->addText('companyName', 'companyName')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		$this->addText('streetSplit', 'street')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		$this->addText('city', 'city')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		$this->addText('zip', 'zip')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		$this->addText('country', 'country')
			->setAttribute('class', 'form-control-plaintext w-100')
			->setAttribute('readonly');
		$this->addHidden('street');
		$this->addHidden('streetNumber');
		$this->addHidden('orientationNumber');
		$this->addHidden('countryId');

		$this->setDefaults($this->getDefaults());
	}


	/**
	 * @return array
	 */
	private function getDefaults()
	{
		return [
			'ic' => $this->user->identity->address->vat,
			'dic' => $this->user->identity->address->euvat,
			'icDph' => $this->user->identity->address->getEuVatSk(),
			'companyName' => $this->user->identity->address->name,
			'street' => $this->user->identity->address->street,
			'streetNumber' => $this->user->identity->address->houseNumber,
			'orientationNumber' => $this->user->identity->address->orientationNumber,
			'streetSplit' => FormatHelper::splitStreet($this->user->identity->address->street, $this->user->identity->address->houseNumber, $this->user->identity->address->orientationNumber),
			'city' => $this->user->identity->address->city,
			'zip' => $this->user->identity->address->zipPostalCode,
			'country' => $this->translator->translate('country.' . $this->user->identity->address->country),
			'countryId' => $this->countryRepository->getCountryId($this->user->identity->address->country)
		];
	}

}
