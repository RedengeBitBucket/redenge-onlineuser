<?php

namespace Redenge\OnlineUser\FrontModule\Components\Cart;


/**
 * Description of ICartInvoice
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICartInvoiceControl
{

	/**
	 * @return CartInvoiceControl
	 */
	function create();

}
