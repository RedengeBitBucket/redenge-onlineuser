<?php

namespace Redenge\OnlineUser\FrontModule\Components\Cart;

use Kdyby\Translation\Translator;
use Nette\Application\Responses\TextResponse;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Components\Cart\Containers\PersonalContainer;
use Redenge\OnlineUser\FrontModule\Components\Cart\Containers\BillingContainer;
use Redenge\OnlineUser\FrontModule\Components\Cart\Containers\DeliveryContainer;
use Redenge\OnlineUser\FrontModule\Entity\Address;
use Redenge\OnlineUser\FrontModule\Entity\Repository\CountryRepository;
use Redenge\OnlineUser\FrontModule\Exceptions\ResponseException;
use Redenge\OnlineUser\FrontModule\Tools\FormatHelper;


/**
 * Description of CartInvoice
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CartInvoiceControl extends Control
{

	/**
	 * @var string
	 */
	private $action;

	/**
	 * @var string
	 */
	private $backLink;

	/**
	 * @var callable[]
	 */
	public $onTermsSet = [];

	/**
	 * @var App
	 */
	protected $apiUser;

	/**
	 * @var Translator
	 */
	protected $translator;

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var Environment
	 */
	protected $environment;

	/**
	 * @var CountryRepository
	 */
	protected $countryRepository;

	/**
	 * @var IChangeDeliveryAddressControl
	 */
	protected $changeDeliveryAddressFactory;

	/**
	 * @var callable
	 */
	public $onSaveDeliveryAddress = [];

	/**
	 * @var callable
	 */
	public $onChangePhone = [];


	public function __construct
	(
		AppFactory $apiUserFactory,
		Translator $translator,
		User $user,
		Environment $environment,
		CountryRepository $countryRepository,
		IChangeDeliveryAddressControl $changeDeliveryAddressFactory)
	{
		$this->apiUser = $apiUserFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->translator = $translator;
		$this->user = $user;
		$this->environment = $environment;
		$this->countryRepository = $countryRepository;
		$this->changeDeliveryAddressFactory = $changeDeliveryAddressFactory;
	}


	/**
	 * @param string $action
	 * @return \Redenge\OnlineUser\FrontModule\Components\Cart\CartInvoiceControl
	 */
	public function setAction($action)
	{
		$this->action = $action;
		return $this;
	}


	/**
	 * @param string $backLink
	 * @return \Redenge\OnlineUser\FrontModule\Components\Cart\CartInvoiceControl
	 */
	public function setBackLink($backLink)
	{
		$this->backLink = $backLink;
		return $this;
	}


	/**
	 * @return Form
	 */
	public function createComponentForm()
	{
		$form = new Form;
		$form->setAction($this->action)
			->setTranslator($this->translator->domain('onlineUser.cartInvoice'))
			->getElementPrototype()->appendAttribute('class', 'onlineUser-form cartInvoice');

		$form['personalContainer'] = new PersonalContainer($this, $this->user, $this->environment, $this->countryRepository);
		$form['billingContainer'] = new BillingContainer($this->user, $this->environment, $this->translator, $this->countryRepository);
		$form['deliveryContainer'] = new DeliveryContainer($this, $this->user, $this->translator, $this->countryRepository);

		$form->addTextArea('note', 'note', null, 5)
			->setAttribute('class', 'form-control');

		$form->addHtml('terms', 'terms')
			->setAttribute('class', 'terms-conditions');

		$this->onTermsSet($form['terms']);

		$form->addSubmit('saveInvoice', 'continue_order')
			->setAttribute('class', 'btn btn-next');

		if (!empty($this->backLink)) {
			$form->addButton('back', 'back')
				->setAttribute('class', 'btn btn-back')
				->setAttribute('onclick', sprintf('window.location.href="%s"', $this->backLink));
		}

		$form->addCheckbox('agreement', 'agreement')
			->setRequired('//This field is required.')
			->setAttribute('form-control');

		return $form;
	}


	/**
	 * Formulář pro změnu doručovací adresy
	 *
	 * @return ChangeDeliveryAddressControl
	 */
	public function createComponentDeliveryAddressForm()
	{
		$control = $this->changeDeliveryAddressFactory->create();
		$control->setCloseModalOnSuccess(true);
		$control->onSaveDeliveryAddress[] = function (Address $address) {
			$this->onSaveDeliveryAddress($address);
			$this->user->identity->setDeliveryAddress($address);
			$this['form'] = $this->createComponentForm();
			$this['form']['deliveryContainer']->setAddress($address);
			$this->redrawControl('containers');
		};

		return $control;
	}


	/**
	 * Zobrazení formuláře pro změnu doručovací adresy
	 */
	public function handleShowDeliveryAddressForm()
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/showDeliveryAddressForm.latte');

		$response = new TextResponse($template);

		$this->presenter->sendResponse($response);
	}


	public function handleChangePhone($type)
	{
		$personalContainer = $this->getParameter('personalContainer', []);
		list($prefix, $phone) = array_values($personalContainer);
		if ($prefix && $phone) {
			try {
				$phone = FormatHelper::implodePhone($prefix, $phone);
				$result = $this->apiUser->updateConnectionForRelation($this->user->id, $phone, $type);
				$this->user->identity->{$type} = $phone;
				$this->onChangePhone($phone);
				$this->presenter->payload->success = true;
			} catch (ResponseException $e) {
				$this->presenter->payload->success = false;
			}
		} else {
			$this->presenter->payload->success = false;
		}

		$this->presenter->sendPayload();	
	}


	public function render()
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/cartInvoiceControl.latte');

		$template->render();
	}


	public function __toString()
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/cartInvoiceControl.latte');

		return (string) $template;
	}
}
