<?php

namespace Redenge\OnlineUser\FrontModule\Components\Cart;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Kdyby\Translation\Translator;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Entity\Address;
use Redenge\OnlineUser\FrontModule\Entity\Repository\CountryRepository;
use Redenge\OnlineUser\FrontModule\Exceptions\ResponseException;
use Redenge\OnlineUser\FrontModule\Exceptions\SuccessException;
use Redenge\OnlineUser\FrontModule\Tools\FormatHelper;


/**
 * Description of ChangeDeliveryAddressControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class ChangeDeliveryAddressControl extends Control
{

	/**
	 * @var App
	 */
	protected $apiUser;

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var Translator
	 */
	protected $translator;

	/**
	 * @var Environment
	 */
	protected $environment;


	/**
	 * @var CountryRepository
	 */
	protected $countryRepository;

	/**
	 * @var callable
	 */
	public $onSaveDeliveryAddress = [];

	/**
	 * @var int
	 */
	private $organizationNumber;

	/**
	 * @var int
	 */
	private $contactNumber;

	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var string
	 */
	private $phone;

	/**
	 * @var string
	 */
	private $mobile;

	/**
	 * Po úspěšném založení nové adresy zavře modální okno
	 *
	 * @var bool
	 */
	private $closeModalOnSuccess = false;


	public function __construct
	(
		AppFactory $apiUserFactory,
		User $user,
		Translator $translator,
		Environment $environment,
		CountryRepository $countryRepository
	)
	{
		$this->apiUser = $apiUserFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->user = $user;
		$this->translator = $translator;
		$this->environment = $environment;
		$this->countryRepository = $countryRepository;
		$this->organizationNumber = $user->identity->address->organizationNumber;
		$this->contactNumber = $user->identity->contactNumber;
		$this->email = $user->identity->email;
		$this->phone = $user->identity->phone;
		$this->mobile = $user->identity->mobile;
	}


	public function setOrganizationNumber($organizationNumber)
	{
		$this->organizationNumber = $organizationNumber;
	}


	public function setContactNumber($contactNumber)
	{
		$this->contactNumber = $contactNumber;
	}


	public function setEmail($email)
	{
		$this->email = $email;
	}


	public function setPhone($phone)
	{
		$this->phone = $phone;
	}


	public function setMobile($mobile)
	{
		$this->mobile = $mobile;
	}


	public function setCloseModalOnSuccess($bool)
	{
		$this->closeModalOnSuccess = $bool;
	}


	public function createComponentForm()
	{
		$address = $this->apiUser->getDeliveryAddresses($this->organizationNumber);

		$form = new Form;
		$form->setTranslator($this->translator->domain('onlineUser.changeDeliveryAddress'));

		$form->addSelect('address', null, $this->formatAddress($address))
			->setAttribute('class', 'form-control')
			->setAttribute('data-select-model', $this->getJsonModel($address))
			->setPrompt($this->translator->translate('onlineUser.changeDeliveryAddress.preFillOfExistingAddresses'))
			->setTranslator(null);

		$form->addText('name', 'nameFirstnameLastname')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->getLabelPrototype()->appendAttribute('class', 'required');;

		$form->addText('street', 'street')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->setAttribute('autocomplete', 'off')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addText('streetNumber', 'streetNumber')
			->setAttribute('class', 'form-control')
			->setAttribute('autocomplete', 'off');

		$form->addText('orientationNumber', 'orientationNumber')
			->setAttribute('class', 'form-control')
			->setAttribute('autocomplete', 'off');

		$form->addText('city', 'city')
			->setAttribute('class', 'form-control')
			->setRequired('//This field is required.')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addText('zip', 'zip')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->addRule(Form::PATTERN, '//onlineUser.validation.zip_not_valid', '^([\d]{3})( )?([\d]{2})')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addSelect('countryIso', $this->translator->translate('onlineUser.registration.country'), $this->countryRepository->getAssignedCountries($this->environment->profileCode))
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->setTranslator(NULL)
			->setValue($this->environment->countryIso)
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addCheckbox('default', 'chooseDefault');

		$form->addSubmit('send', 'save')
			->setAttribute('class', 'ajax btn btn-default')
			->setAttribute('data-spinner-target', '#addressList .onlineUser-form');

		$form->addHidden('organizationNumber', $this->organizationNumber);
		$form->addHidden('contactNumber', $this->contactNumber);
		$form->addHidden('email', $this->email);
		$form->addHidden('mobile', $this->mobile);
		$form->addHidden('phone', $this->phone);

		$form->onSuccess[] = [$this, 'onSuccessForm'];
		$form->onError[] = [$this, 'onErrorForm'];

		return $form;
	}


	public function onSuccessForm(Form $form)
	{
		$values = $form->values;

		$address = new Address;
		$address->name = $values->name;
		$address->street = $values->street;
		$address->houseNumber = $values->streetNumber;
		$address->orientationNumber = $values->orientationNumber;
		$address->city = $values->city;
		$address->zipPostalCode = $values->zip;
		$address->country = $values->countryIso;
		$address->setIsDefaultDeliveryAddress($values->default);

		try {
			$result = $this->apiUser->createNewDeliveryAddress(
				$values->organizationNumber,
				$values->contactNumber,
				$values->name,
				$values->street,
				$values->city,
				$values->countryIso,
				$values->zip,
				$values->default,
				$values->email,
				$values->phone,
				$values->mobile,
				$values->streetNumber,
				$values->orientationNumber
			);
			$this->apiUser->cleanByTag(App::CACHE_TAG_GET_DELIVERY_ADDRESSES);
			$this->onSaveDeliveryAddress($address);

			if ($this->closeModalOnSuccess) {
				$this->presenter->payload->closeModal = true;
			} else {
				$this->flashMessage('onlineUser.changeDeliveryAddress.success', 'success');
				$this->redrawControl('flashes');
			}
		} catch (ResponseException $e) {
			$data = $e->getResponseData();
			if (isset($data['AddressExists']) && $data['AddressExists'] === true) {
				$this->onSaveDeliveryAddress($address);
				if ($this->closeModalOnSuccess) {
					$this->presenter->payload->closeModal = true;
				} else {
					$this->flashMessage('onlineUser.changeDeliveryAddress.addressExists', 'warning');
					$this->redrawControl('flashes');
				}
			} else {
				$preException = $e->getPrevious();
				if ($preException instanceof SuccessException) {
					$this->flashMessage($preException->getMessage(), 'danger');
				} else {
					$this->flashMessage('onlineUser.changeDeliveryAddress.error', 'danger');
				}
				$this->redrawControl('flashes');
			}
		}
	}


	public function onErrorForm(Form $form)
	{
		$this->flashMessage('onlineUser.validation.form_not_valid', 'danger');
		$this->redrawControl('formWrapper');
	}


	private function formatAddress($address)
	{
		$return = [];
		foreach ($address as $item) {
			$return[$item['OrganizationNumber']] = $item['Name']
				. ', ' . FormatHelper::splitStreet($item['Street'], $item['HouseNumber'], $item['OrientationNumber'])
				. ', ' . $item['City']
				. ', ' . $this->translator->translate('country.' . $item['Country']);
		}

		return $return;
	}


	private function getJsonModel($address)
	{
		$return = [];
		foreach ($address as $item) {
			$row = new \stdClass();
			$row->name = $item['Name'];
			$row->street = $item['Street'];
			$row->streetNumber = $item['HouseNumber'];
			$row->orientationNumber = $item['OrientationNumber'];
			$row->city = $item['City'];
			$row->zip = $item['ZipPostalCode'];
			$row->countryIso = $item['Country'];

			$return[$item['OrganizationNumber']] = $row;
		}

		return json_encode($return);
	}


	public function render()
	{
		$this->template->setFile(__DIR__ . '/templates/changeDeliveryAddressControl.latte');
		$this->template->render();
	}

}
