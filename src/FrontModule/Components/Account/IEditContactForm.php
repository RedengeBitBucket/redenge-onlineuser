<?php

namespace Redenge\OnlineUser\FrontModule\Components\Account;

/**
 * Description of IEditContactForm
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IEditContactForm
{

	/**
	 * @return EditContactForm
	 */
	function create();

}
