<?php

namespace Redenge\OnlineUser\FrontModule\Components\Account;

use Kdyby\Translation\Translator;
use Nette\Application\Responses\TextResponse;
use Nette\Application\UI\Control;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Redenge\OnlineUser\FrontModule\Entity;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Components\Account\IEditContactForm;
use Redenge\OnlineUser\FrontModule\Components\Account\INewUserControl;
use Redenge\OnlineUser\FrontModule\Components\Cart\IChangeDeliveryAddressControl;
use Redenge\OnlineUser\FrontModule\Entity\Repository\CountryRepository;
use Redenge\OnlineUser\FrontModule\Tools\FormatHelper;
use Ublaboo\DataGrid\DataGrid;


/**
 * Description of UserListControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class UserListControl extends Control
{

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var App
	 */
	protected $apiUser;

	/**
	 * @var Translator
	 */
	protected $translator;

	/**
	 * @var CountryRepository
	 */
	protected $countryRepository;

	/**
	 * @var INewUserControl
	 */
	protected $newUserFactory;

	/**
	 * @var IChangeDeliveryAddressControl
	 */
	protected $changeDeliveryAddressFactory;

	/**
	 * @var IEditContactForm
	 */
	protected $editContactFactory;


	public function __construct(
		User $user,
		AppFactory $apiUserFactory,
		Environment $environment,
		Translator $translator,
		CountryRepository $countryRepository,
		INewUserControl $newUserFactory,
		IChangeDeliveryAddressControl $changeDeliveryAddressFactory,
		IEditContactForm $editContactFactory
	)
	{
		$this->user = $user;
		$this->apiUser = $apiUserFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->translator = $translator;
		$this->countryRepository = $countryRepository;
		$this->newUserFactory = $newUserFactory;
		$this->changeDeliveryAddressFactory = $changeDeliveryAddressFactory;
		$this->editContactFactory = $editContactFactory;
	}


	public function attached($presenter)
	{
		if (!$this->user->isInRole(Entity\User::ROLE_ADMIN)) {
			$presenter->redirectUrl('/');
		}
		parent::attached($presenter);
	}


	/**
	 * @return NewUserControl
	 */
	public function createComponentNewUserForm()
	{
		$control = $this->newUserFactory->create();
		$control->onSuccess[] = function() {
			$this['grid']->setDataSource($this->getData());
			$this['grid']->reload();
		};

		return $control;
	}


	public function createComponentGrid()
	{
		$source = $this->getData();

		$grid = new DataGrid();

		$grid->setTranslator($this->translator->domain('onlineUser.account'));
		$grid->setPrimaryKey('ID');
		$grid->setDataSource($source);

		$grid->addColumnText('contactPerson', 'contactPerson')
			->setSortable();
		$grid->addColumnText('Function', 'function')
			->setSortable();
		$grid->addColumnText('Role', 'role')
			->setSortable();
		$grid->addColumnText('Phone', 'phone')
			->setSortable();
		$grid->addColumnText('Mobile', 'mobile')
			->setSortable();
		$grid->addColumnText('defaultDeliveryAddress', 'defaultDeliveryAddress')
			->setRenderer(function ($item) {
				return Html::el('span')->setHtml($item['defaultDeliveryAddress']);
			});
		$grid->addColumnText('editContact', 'editContact')
			->setRenderer(function ($item) {
				$el = Html::el('a', [
					'data-toggle' => 'modal',
					'data-target' => '#contactForm',
					'href' => $this->link('showContactForm!', $item['ID'], $item['Email'], $item['DegreeBefore'],
						$item['Name'], $item['Surname'], $item['DegreeAfter'], $item['Phone'], $item['Mobile'],
						$item['Role'], $item['Function'])
				]);

				return $el->setText($this->translator->translate('onlineUser.account.editContact'));
			});
		/*$grid->addColumnText('addAddress', 'addAddress')
			->setRenderer(function ($item) {
				$el = Html::el('a', [
					'data-toggle' => 'modal',
					'data-target' => '#addressList',
					'href' => $this->link('showDeliveryAddressForm!', $item['OrganizationNumber'], $item['ID'], $item['Email'], $item['Mobile'], $item['Phone'])
				]);

				return $el->setText($this->translator->translate('onlineUser.account.addAddress'));
			});*/

		$grid->addToolbarButton('addUser!', 'addNewUser')->addAttributes([
			'data-toggle' => "modal",
			'data-target' => "#modalNewUser",
			'href' => $this->link('showNewUserForm!')
		])->setClass('btn btn-success btn-xs');

		return $grid;
	}


	private function getData()
	{
		$source = $this->apiUser->getContactPersonsForInvoiceOrganizationByAdmin($this->user->identity->address->organizationNumber);
		foreach ($source as $i => $item) {
			$source[$i]['contactPerson'] = (!empty($item['DegreeBefore']) ? $item['DegreeBefore'] . ' ' : '')
			. $item['Name']
			. ' ' . $item['Surname']
			. (!empty($item['DegreeAfter']) ? ' ' . $item['DegreeAfter'] : '');

			$defaultDeliveryAddress = $this->apiUser->getRelationDeliveryOrganization($item['ID']);
			$source[$i]['defaultDeliveryAddress'] = $defaultDeliveryAddress['Street']
				. ' ' . $defaultDeliveryAddress['HouseNumber']
				. (!empty($defaultDeliveryAddress['OrientationNumber']) ? '/' . $defaultDeliveryAddress['OrientationNumber'] : '')
				. '<br>' . $defaultDeliveryAddress['ZipPostalCode']
				. ' ' . $defaultDeliveryAddress['City'];
		}

		return $source;
	}


	/**
	 * Formulář pro změnu doručovací adresy
	 *
	 * @return ChangeDeliveryAddressControl
	 */
	public function createComponentDeliveryAddressForm()
	{
		$control = $this->changeDeliveryAddressFactory->create();
		$control->setOrganizationNumber($this->getParameter('organizationNumber'));
		$control->setContactNumber($this->getParameter('contactNumber'));
		$control->setEmail($this->getParameter('email', ''));
		$control->setMobile($this->getParameter('mobile', ''));
		$control->setPhone($this->getParameter('phone', ''));

		return $control;
	}


	public function createComponentContactForm()
	{
		$control = $this->editContactFactory->create();
		list($phonePrefix, $phone) = FormatHelper::splitPhone($this->getParameter('phone', ''));
		list($mobilePrefix, $mobile) = FormatHelper::splitPhone($this->getParameter('mobile', ''));
		$control = $this->editContactFactory->create();
		$control->setDefaults([
			'email' => $this->getParameter('email', 0),
			'contactId' => $this->getParameter('contactId', 0),
			'titlePrefix' => $this->getParameter('titlePrefix', ''),
			'name' => $this->getParameter('name', ''),
			'lastname' => $this->getParameter('lastname', ''),
			'titleSuffix' => $this->getParameter('titleSuffix', ''),
			'phonePrefix' => $phonePrefix,
			'phone' => $phone,
			'mobilePrefix' => $mobilePrefix,
			'mobile' => $mobile,
			'role' => $this->getParameter('role'),
			'function' => $this->getParameter('function', '')
		]);
		$control->onSuccess[] = function(ArrayHash $values) {
			$this->apiUser->updateContactPersonByAdmin($values->contactId, $values->email, $values->name, $values->lastname,
				$values->mobilePrefix, $values->mobile, $values->titlePrefix, $values->titleSuffix, $values->phonePrefix, $values->phone);
			$this->apiUser->cleanByTag(App::CACHE_TAG_GET_CONTACT_PERSONS_FOR_INVOICE_ORGANIZATION_BY_ADMIN);

			$this['grid']->setDataSource($this->getData());
			$this['grid']->reload();
		};

		return $control;
	}

	/**
	 * Zobrazení formuláře pro přidání nového uživatele
	 */
	public function handleShowNewUserForm()
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/showNewUserForm.latte');

		$response = new TextResponse($template);

		$this->presenter->sendResponse($response);
	}


	/**
	 * Editace kontaktu
	 *
	 */
	public function handleShowContactForm($contactId, $email, $titlePrefix, $name, $lastname, $titleSuffix, $phone, $mobile, $role, $function)
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/showContactForm.latte');

		$response = new TextResponse($template);

		$this->presenter->sendResponse($response);
	}


	/**
	 * Přidání nové adresy
	 *
	 * @param int $contactId Id vztahu
	 */
	public function handleShowDeliveryAddressForm($organizationNumber, $contactNumber, $email, $mobile, $phone)
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/../Cart/templates/showDeliveryAddressForm.latte');

		$response = new TextResponse($template);

		$this->presenter->sendResponse($response);
	}


	public function handleAddUser()
	{

	}


	public function render()
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/listControl.latte');

		$template->render();
	}


	public function __toString()
	{
		$template = $this->getTemplate();
		$template->setFile(__DIR__ . '/templates/listControl.latte');

		return (string) $template;
	}

}
