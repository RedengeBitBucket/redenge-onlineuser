<?php

namespace Redenge\OnlineUser\FrontModule\Components\Account;

/**
 * Description of IUserListControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IUserListControl
{

	/**
	 * @return UserListControl
	 */
	function create();

}
