<?php

namespace Redenge\OnlineUser\FrontModule\Components\Account;

/**
 * Description of INewUserControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface INewUserControl
{

	/**
	 * @return NewUserControl
	 */
	function create();

}
