<?php

namespace Redenge\OnlineUser\FrontModule\Components\Account;

use Exception;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Entity\Repository\CountryRepository;
use Redenge\OnlineUser\FrontModule\Exceptions\ResponseException;
use Redenge\OnlineUser\FrontModule\Exceptions\SuccessException;


/**
 * Description of NewUserControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class NewUserControl extends Control
{

	/**
	 * @var App
	 */
	private $apiUser;

	/**
	 * @var Environment
	 */
	private $environment;

	/**
	 * @var User
	 */
	private $user;

	/**
	 * @var CountryRepository
	 */
	private $countryRepository;

	/**
	 * @var Translator
	 */
	private $translator;

	/**
	 * @var \Nette\Application\UI\Presenter
	 */
	private $presenter;

	/**
	 * @var callable
	 */
	public $onSuccess = [];


	public function __construct(AppFactory $apiUserFactory, Environment $environment, User $user, CountryRepository $countryRepository, Translator $translator)
	{
		$this->apiUser = $apiUserFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->environment = $environment;
		$this->user = $user;
		$this->countryRepository = $countryRepository;
		$this->translator = $translator;
	}


	public function attached($presenter)
	{
		$this->presenter = $presenter;
		
		parent::attached($presenter);
	}


	public function createComponentForm()
	{
		$countryPrefixes = $this->countryRepository->getMobilePrefixes();
		$mobilePrefixes = $this->countryRepository->formatMobilePrefixes($countryPrefixes);

		$form = new Form;
		$form->setTranslator($this->translator->domain('onlineUser.account.newUser'));

		$form->addText('name', 'name')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addText('lastname', 'lastname')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addText('titlePrefix', 'title_prefix')
			->setAttribute('class', 'form-control');

		$form->addText('titleSuffix', 'title_suffix')
			->setAttribute('class', 'form-control');

		$form->addText('email', 'email')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->addRule(Form::EMAIL, '//Please enter a valid email address.')
			->addRule('Redenge\OnlineUser\FrontModule\Tools\ValidationHelper::validateEmailNotExists',
				'//onlineUser.validation.email_exists', ['link' => $this->presenter->link(':OnlineUser:Front:Validation:validateEmailNotExists'), 'api' => $this->apiUser])
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addSelect('mobilePrefix', NULL, $mobilePrefixes)
			->setAttribute('class', 'form-control')
			->setTranslator(NULL);

		$form->addText('mobile', 'mobile')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->addRule(Form::PATTERN, '//onlineUser.validation.phoneFormat', '[\d ]+')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addSelect('phonePrefix', NULL, $mobilePrefixes)
			->setAttribute('class', 'form-control')
			->setTranslator(NULL);

		$form->addText('phone', 'phone')
			->setAttribute('class', 'form-control')
			->addConditionOn($form['phone'], Form::FILLED)
				->addRule(Form::PATTERN, '//onlineUser.validation.phoneFormat', '[\d ]+');

		$form->addSelect('function', $this->translator->translate('onlineUser.account.newUser.function'), $this->getFunctions())
			->setAttribute('class', 'form-control')
			->setTranslator(NULL);

		$form->addSubmit('save', 'save')
			->setAttribute('class', 'ajax btn btn-default mr-2')
			->setAttribute('data-spinner-class', 'ajax-spinner-inline')
			->setAttribute('data-spinner-target', '.onlineUser-form .send-button')
			->setAttribute('data-spinner-place', 'append');

		$form->onSuccess[] = [$this, 'onSuccessForm'];

		if (isset($countryPrefixes[strtoupper($this->environment->countryIso)])) {
			$form['mobilePrefix']->setValue($countryPrefixes[strtoupper($this->environment->countryIso)]);
			$form['phonePrefix']->setValue($countryPrefixes[strtoupper($this->environment->countryIso)]);
		}

		return $form;
	}


	/**
	 * @return array
	 */
	public function getFunctions()
	{
		$return = [];
		foreach ($this->apiUser->getSettings()->getFunctions($this->environment->languageIso) as $function) {
			$return[$function] = $function;
		}

		return $return;
	}


	public function onSuccessForm(Form $form)
	{
		try {
			$values = $form->values;
			$this->apiUser->createNewUser($this->user->identity->address->organizationNumber, $values->name, $values->lastname,
				$values->email, $values->mobilePrefix, $values->mobile, $values->function, $values->titlePrefix,
				$values->titleSuffix, $values->phonePrefix, $values->phone);
			$this->apiUser->cleanByTag(App::CACHE_TAG_GET_CONTACT_PERSONS_FOR_INVOICE_ORGANIZATION_BY_ADMIN);
			$this->onSuccess();
			$this->flashMessage('onlineUser.account.newUser.onSuccess', 'success');
		} catch (ResponseException $e) {
			bdump($e);
			$preException = $e->getPrevious();
            if ($preException instanceof SuccessException) {
                $this->flashMessage($preException->getMessage(), 'danger');
            } else {
				$this->flashMessage('onlineUser.account.newUser.onError', 'danger');
			}
		} catch (Exception $e) {
			$this->flashMessage('onlineUser.account.newUser.onError', 'danger');
		}

		$this->redrawControl('flashes');
	}


	public function render()
	{
		$this->template->setFile(__DIR__ . '/templates/newUserControl.latte');
		$this->template->render();
	}

}
