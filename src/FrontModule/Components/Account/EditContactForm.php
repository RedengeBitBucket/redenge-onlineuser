<?php

namespace Redenge\OnlineUser\FrontModule\Components\Account;

use Exception;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Entity\Repository\CountryRepository;
use Redenge\OnlineUser\FrontModule\Exceptions\ResponseException;
use Redenge\OnlineUser\FrontModule\Exceptions\SuccessException;
use Redenge\OnlineUser\FrontModule\Tools\FormatHelper;


/**
 * Description of EditContactForm
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class EditContactForm extends Control
{

	/**
	 * @var App
	 */
	private $apiUser;

	/**
	 * @var Environment
	 */
	private $environment;

	/**
	 * @var User
	 */
	private $user;

	/**
	 * @var CountryRepository
	 */
	private $countryRepository;

	/**
	 * @var Translator
	 */
	private $translator;

	/**
	 * @var array
	 */
	private $defaults;

	/**
	 * @var callable
	 */
	public $onSuccess = [];


	public function __construct(AppFactory $apiUserFactory, Environment $environment, User $user, CountryRepository $countryRepository, Translator $translator)
	{
		$this->apiUser = $apiUserFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->environment = $environment;
		$this->user = $user;
		$this->countryRepository = $countryRepository;
		$this->translator = $translator;
	}


	public function createComponentForm()
	{
		$countryPrefixes = $this->countryRepository->getMobilePrefixes();
		$mobilePrefixes = $this->countryRepository->formatMobilePrefixes($countryPrefixes);

		$form = new Form;
		$form->setTranslator($this->translator->domain('onlineUser.account.profile'));

		$form->addText('titlePrefix', 'titlePrefix')
			->setAttribute('class', 'form-control');

		$form->addText('name', 'name')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addText('lastname', 'lastname')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addText('titleSuffix', 'titleSuffix')
			->setAttribute('class', 'form-control');

		$form->addSelect('mobilePrefix', NULL, $mobilePrefixes)
			->setAttribute('class', 'form-control')
			->setTranslator(NULL);

		$form->addText('mobile', 'mobile')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->addRule(Form::PATTERN, '//onlineUser.validation.phoneFormat', '[\d ]+')
			->getLabelPrototype()->appendAttribute('class', 'required');

		$form->addSelect('phonePrefix', NULL, $mobilePrefixes)
			->setAttribute('class', 'form-control')
			->setTranslator(NULL);

		$form->addText('phone', 'phone')
			->setAttribute('class', 'form-control')
			->addConditionOn($form['phone'], Form::FILLED)
				->addRule(Form::PATTERN, '//onlineUser.validation.phoneFormat', '[\d ]+');

		$form->addHidden('email');
		$form->addHidden('contactId');
		$form->addHidden('role');
		$form->addHidden('function');

		$form->addSubmit('save', 'confirm')
			->setAttribute('class', 'ajax btn btn-default mr-2')
			->setAttribute('data-spinner-class', 'ajax-spinner-inline')
			->setAttribute('data-spinner-target', '.onlineUser-form .send-button')
			->setAttribute('data-spinner-place', 'append');

		if (isset($countryPrefixes[strtoupper($this->environment->countryIso)])) {
			$form['mobilePrefix']->setValue($countryPrefixes[strtoupper($this->environment->countryIso)]);
			$form['phonePrefix']->setValue($countryPrefixes[strtoupper($this->environment->countryIso)]);
		}

		$form->setDefaults($this->defaults);

		$form->onSuccess[] = [$this, 'onSuccessForm'];

		return $form;
	}


	public function setDefaults(array $defaults)
	{
		$this->defaults = $defaults;
	}


	public function onSuccessForm(Form $form)
	{
		try {
			$this->onSuccess($form->values);
			$this->flashMessage('onlineUser.account.profile.updateSuccess', 'success');
		} catch (ResponseException $e) {
			$preException = $e->getPrevious();
            if ($preException instanceof SuccessException) {
                $this->flashMessage($preException->getMessage(), 'danger');
            } else {
				$this->flashMessage('onlineUser.account.profile.updateError', 'danger');
			}
		} catch (Exception $e) {
			$this->flashMessage('onlineUser.account.account.updateError', 'danger');
		}

		$this->redrawControl('flashes');
	}


	public function render()
	{
		$this->template->setFile(__DIR__ . '/templates/editContactForm.latte');
		$this->template->render();
	}
}
