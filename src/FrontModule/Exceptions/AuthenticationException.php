<?php

namespace Redenge\OnlineUser\FrontModule\Exceptions;

/**
 * Description of AuthorizationException
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class AuthenticationException extends ApiRedengeException
{
}
