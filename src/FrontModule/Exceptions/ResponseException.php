<?php

namespace Redenge\OnlineUser\FrontModule\Exceptions;

use Redenge\OnlineUser\FrontModule\Response;


/**
 * Description of ResponseException
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class ResponseException extends ApiRedengeException
{

	/**
	 * @var Response The response that threw the exception.
	 */
	protected $response;

	/**
	 * @var array Decoded response.
	 */
	protected $responseData;


	/**
	 * 
	 * @param Response $response
	 * @param ApiRedengeException $previousException
	 */
	public function __construct(Response $response, ApiRedengeException $previousException = null)
	{
		$this->response = $response;
		$this->responseData = $response->getDecodedBody();

		$errorMessage = $this->get('Message', 'Unknown error from API Redenge.');
		$errorCode = $this->get('Code', -1);
		$errorCode = !is_numeric($errorCode) ? 0 : $errorCode;

		parent::__construct($errorMessage, $errorCode, $previousException);
	}


	/**
	 * A factory for creating the appropriate exception based on the response from API Redenge.
	 *
	 * @param Response $response The response that threw the exception.
	 *
	 * @return ResponseException
	 */
	public static function create(Response $response)
	{
		$statusCode = $response->getHttpStatusCode();
		$code = null;
		$message = 'Unknown error from API Redenge.';

		if ($statusCode === 401) {
			$message = 'Nepodařilo se připojit na vzdálené API. Vzdálený API server je nedostupný nebo jsou nesprávné přihlašovací údaje';
			return new static($response, new AuthenticationException($message, 401));
		} elseif ($statusCode === 200) {
			$data = $response->getDecodedBody();

			if (isset($data['Errors']) && count($data['Errors']) > 0) {
				$error = current($data['Errors']);
				$code = (int) $error['Code'];
				$message = $error['Message'];

				return new static($response, new FieldException($message, $code));
			} elseif (isset($data['Success']) && $data['Success'] === false) {
				$message = isset($data['Message']) ? $data['Message'] : 'Neznámá chyba';

				return new static($response, new SuccessException($message, $code));
			}
		}

		return new static($response, new OtherException($message, $code));
	}


	/**
	 * Checks isset and returns that or a default value.
	 *
	 * @param string $key
	 * @param mixed  $default
	 *
	 * @return mixed
	 */
	private function get($key, $default = null)
	{
		if (isset($this->responseData[$key])) {
			return $this->responseData[$key];
		}

		return $default;
	}


	/**
	 * Returns the HTTP status code
	 *
	 * @return int
	 */
	public function getHttpStatusCode()
	{
		return $this->response->getHttpStatusCode();
	}


	/**
	 * Returns the sub-error code
	 *
	 * @return int
	 */
	public function getSubErrorCode()
	{
		return $this->get('error_subcode', -1);
	}


	/**
	 * Returns the error type
	 *
	 * @return string
	 */
	public function getErrorType()
	{
		return $this->get('type', '');
	}


	/**
	 * Returns the raw response used to create the exception.
	 *
	 * @return string
	 */
	public function getRawResponse()
	{
		return $this->response->getBody();
	}


	/**
	 * Returns the decoded response used to create the exception.
	 *
	 * @return array
	 */
	public function getResponseData()
	{
		return $this->responseData;
	}


	/**
	 * Returns the response entity used to create the exception.
	 *
	 * @return FacebookResponse
	 */
	public function getResponse()
	{
		return $this->response;
	}

}
