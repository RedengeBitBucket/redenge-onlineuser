<?php

namespace Redenge\OnlineUser\FrontModule\Exceptions;

/**
 * Description of SuccessException
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class SuccessException extends ApiRedengeException
{
}
