<?php

namespace Redenge\OnlineUser\FrontModule\Exceptions;

/**
 * Description of FieldException
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class FieldException extends ApiRedengeException
{
}
