<?php

namespace Redenge\OnlineUser\FrontModule\Exceptions;

use Exception;

/**
 * Description of ApiRedengeException
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class ApiRedengeException extends Exception
{
}
