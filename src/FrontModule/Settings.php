<?php

namespace Redenge\OnlineUser\FrontModule;

use Redenge\OnlineUser\FrontModule\HttpClients\HttpClientInterface;


class Settings
{

	/**
	 * @var HttpClientInterface 
	 */
	private $httpClientHandler;

	/**
	 * @var array
	 */
	private $functions;


	/**
	 * @param HttpClientInterface $httpClientHandler
	 * @param array $functions
	 */
	public function __construct($httpClientHandler, $functions)
	{
		$this->httpClientHandler = $httpClientHandler;
		$this->functions = $functions;
	}


	/**
	 * @return HttpClientInterface|null
	 */
	public function getHttpClientHandler()
	{
		return $this->httpClientHandler;
	}


	/**
	 * @param string $languageIso - ISO 639-1
	 * @return array
	 */
	public function getFunctions($languageIso)
	{
		return isset($this->functions[strtolower($languageIso)]) ? $this->functions[strtolower($languageIso)] : [];
	}

}
