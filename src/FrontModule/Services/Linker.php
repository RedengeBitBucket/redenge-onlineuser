<?php

namespace Redenge\OnlineUser\FrontModule\Services;

use Redenge\OnlineUser\FrontModule\Tools\HashHelper;
use Nette\Application\IPresenterFactory;
use Nette\Application\IRouter;
use Nette\Application\LinkGenerator;
use Nette\Http\UrlScript;


/**
 * Description of LinkGenerator
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Linker
{

	const ACTION_ACTIVATION_ACCOUNT = 'OnlineUser:Front:Login:accountActivation';
	const ACTION_CONFIRM_REGISTRATION = 'OnlineUser:Front:Registration:confirmRegistration';
	const ACTION_FORGOT_PASSWORD = 'OnlineUser:Front:Login:changePassword';

	/**
	 * @var LinkGenerator
	 */
	private $linkGenerator;

	/**
	 * @var IRouter
	 */
	private $router;

	/**
	 * @var IPresenterFactory
	 */
	private $presenterFactory;


	public function __construct(LinkGenerator $linkGenerator, IRouter $router, IPresenterFactory $presenterFactory = null)
	{
		$this->linkGenerator = $linkGenerator;
		$this->router = $router;
		$this->presenterFactory = $presenterFactory;
	}


	public function setUrl(UrlScript $url)
	{
		$this->linkGenerator = new LinkGenerator($this->router, $url, $this->presenterFactory);
	}


	/**
	 * @param string $email
	 * @param string $hash DoubleOptionHash
	 *
	 * @return string
	 */
	public function getActivationLink($email, $hash)
	{
		$hash = HashHelper::generateActivationHash($hash, $email);
		return $this->linkGenerator->link(self::ACTION_ACTIVATION_ACCOUNT, ['hash' => $hash]);
	}


	/**
	 * @param string $email
	 * @param string $hash DoubleOptionHash
	 *
	 * @return string
	 */
	public function getActivationRegistrationLink($email, $hash)
	{
		$hash = HashHelper::generateActivationHash($hash, $email);
		return $this->linkGenerator->link(self::ACTION_CONFIRM_REGISTRATION, ['hash' => $hash]);
	}


	/**
	 * @param string $email
	 * @param string $hash DoubleOptionHash
	 *
	 * @return string
	 */
	public function getForgotPasswordLink($email, $hash)
	{
		$hash = HashHelper::generateActivationHash($hash, $email);
		return $this->linkGenerator->link(self::ACTION_FORGOT_PASSWORD, ['hash' => $hash]);
	}

}
