<?php

namespace Redenge\OnlineUser\FrontModule\Services;

use Exception;
use Nette\Security\IAuthenticator;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Entity\User;
use Redenge\OnlineUser\FrontModule\Entity\Address;
use Redenge\OnlineUser\FrontModule\Exceptions\AuthenticationException;
use Redenge\OnlineUser\FrontModule\Exceptions\FieldException;
use Redenge\OnlineUser\FrontModule\Exceptions\OtherException;


/**
 * Description of Authenticator
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Authenticator implements IAuthenticator
{

	/**
	 * @var App
	 */
	private $app;


	public function __construct(AppFactory $appFactory, Environment $environment)
	{
		$this->app = $appFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
	}


	/**
	 * {@inheritdoc}
	 */
	public function authenticate(array $data)
	{
		$user = new User;
		$user->loadFromArray($data[0]);

		$addressData = $this->app->getInvoiceOrganizations([$user->id]);
		$address = new Address();
		$address->loadFromArray($addressData[0]);
		$user->setAddress($address);

		$addressData = $this->app->getRelationDeliveryOrganization($user->id);
		$deliveryAddress = new Address();
		$deliveryAddress->loadFromArray($addressData);
		$user->setDeliveryAddress($deliveryAddress);

		return $user;
	}


	/**
	 * @param string $login
	 * @param string $password
	 *
	 * @return \Nette\Security\Identity
	 */
	private function getIdentity($login, $password)
	{
		try {
		} catch (FieldException $ex) {
			throw new Exception('Některé pole nebylo vyplněno');
		} catch (AuthenticationException $ex) {
			throw new Exception('Technický problém. Kontaktujte prosím podporu');
		} catch (OtherException $ex) {
			throw new Exception('Technický problém. Kontaktujte prosím podporu');
		} catch (\Nette\Security\AuthenticationException $ex) {
			throw new Exception($ex->getMessage());
		}
	}

}
