<?php

namespace Redenge\OnlineUser\FrontModule;

use OutOfBoundsException;


class Provider
{

	/**
	 * @var EnvironmentKey
	 */
	private $environmentKey;

	/**
	 * @var string
	 */
	private $appUrl;

	/**
	 * @var string
	 */
	private $appUsername;

	/**
	 * @var string
	 */
	private $appPassword;


	public function __construct(EnvironmentKey $environmentKey, $appUrl, $appUsername, $appPassword)
	{
		$this->environmentKey = $environmentKey;
		$this->appUrl = $appUrl;
		$this->appUsername = $appUsername;
		$this->appPassword = $appPassword;
	}


	/**
	 * @param EnvironmentKey $key
	 *
	 * @return bool
	 */
	public function matchKey(EnvironmentKey $environmentKey)
	{
		return (string) $environmentKey === (string) $this->environmentKey;
	}


	/**
	 * @return string
	 */
	public function getAppUrl()
	{
		return $this->appUrl;
	}


	/**
	 * @return string
	 */
	public function getAppUsername()
	{
		return $this->appUsername;
	}


	/**
	 * @return string
	 */
	public function getAppPassword()
	{
		return $this->appPassword;
	}


	/**
	 * @return EnvironmentKey
	 */
	public function getEnvironmentKey()
	{
		return $this->environmentKey;
	}

}
