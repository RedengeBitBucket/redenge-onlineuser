<?php

namespace Redenge\OnlineUser\FrontModule;


/**
 * Description of EnvironmentKeyFactory
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class EnvironmentKeyFactory
{

	/**
	 * @param string $multishopCode
	 * @param string $profileCode
	 *
	 * @return EnvironmentKey
	 */
	public static function create($multishopCode, $profileCode)
	{
		return new EnvironmentKey($multishopCode, $profileCode);
	}

}
