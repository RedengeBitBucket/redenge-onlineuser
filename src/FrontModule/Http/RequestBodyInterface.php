<?php

namespace Redenge\OnlineUser\FrontModule\Http;

/**
 * Description of RequestBodyInterface
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface RequestBodyInterface
{

	/**
     * Get the body of the request to send to Graph.
     *
     * @return string
     */
    public function getBody();

}
