<?php

namespace Redenge\OnlineUser\FrontModule\Mail;

use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\Services\Linker;
use Redenge\RetailEmailing\FrontModule\App;
use Redenge\RetailEmailing\FrontModule\AppFactory;
use Redenge\RetailEmailing\FrontModule\EnvironmentKeyFactory;


/**
 * Description of RetailEmailing
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class RetailEmailing implements IMail
{

	const CAMPAIGN_ACTIVATION_ACCOUNT = 'activation';
	const CAMPAIGN_CONFIRM_REGISTRATION = 'confirmRegistration';
	const CAMPAIGN_CONFIRM_REGISTRATION_HEO = 'confirmRegistrationHeO';
	const CAMPAIGN_FORGOT_PASSWORD = 'forgotPassword';

	/**
	 * @var Linker
	 */
	protected $linker;

	/**
	 * @var AppFactory
	 */
	protected $apiFactory;

	/**
	 * @var App
	 */
	protected $api;

	/**
	 * @var string
	 */
	protected $multishopCode;

	/**
	 * @var string
	 */
	protected $countryIso;


	public function __construct(Environment $environment, Linker $linker, AppFactory $retailEmailingFactory)
	{
		$this->linker = $linker;
		$this->apiFactory = $retailEmailingFactory;
		$this->multishopCode = $environment->multishopCode;
		$this->countryIso = $environment->countryIso;
		$this->api = $this->apiFactory->create(
			EnvironmentKeyFactory::create($this->multishopCode, $this->countryIso));
	}


	/**
	 * {@inheritdoc}
	 */
	public function setEnvironment($multishopCode, $countryIso)
	{
		if ($multishopCode !== $this->multishopCode || $countryIso !== $this->countryIso) {
			$this->multishopCode = $multishopCode;
			$this->countryIso = $countryIso;
			$this->api = $this->apiFactory->create(
				EnvironmentKeyFactory::create($multishopCode, $countryIso)
			);
		}
	}


	/**
	 * @return Linker
	 */
	public function getLinker()
	{
		return $this->linker;
	}


	/**
	 * (@inheritdoc}
	 */
	public function sendActivationMail($email, $hash)
	{
		$link = $this->linker->getActivationLink($email, $hash);
		$params = [
			'recipients' => array(
					array (
						"email" => $email
				   )
			),
			"properties" => array(
				"activationLink" => $link
			)
		];
		$this->api->sendCampaign(self::CAMPAIGN_ACTIVATION_ACCOUNT, $params);
	}


	/**
	 * (@inheritdoc}
	 */
	public function sendConfirmRegistrationEmail($email, $hash)
	{
		$link = $this->linker->getActivationRegistrationLink($email, $hash);
		$params = [
			'recipients' => array(
					array (
						"email" => $email
				   )
			),
			"properties" => array(
				"activationLink" => $link
			)
		];

		$this->api->sendCampaign(self::CAMPAIGN_CONFIRM_REGISTRATION, $params);
	}


	/**
	 * (@inheritdoc}
	 */
	public function sendConfirmRegistrationHeOEmail($email, $hash)
	{
		$link = $this->linker->getActivationRegistrationLink($email, $hash);
		$params = [
			'recipients' => array(
					array (
						"email" => $email
				   )
			),
			"properties" => array(
				"activationLink" => $link
			)
		];

		$this->api->sendCampaign(self::CAMPAIGN_CONFIRM_REGISTRATION_HEO, $params);
	}


	public function sendForgotPasswordEmail($email, $hash)
	{
		$link = $this->linker->getForgotPasswordLink($email, $hash);
		$params = [
			'recipients' => array(
					array (
						"email" => $email
				   )
			),
			"properties" => array(
				"lostpassword" => $link
			)
		];

		$this->api->sendCampaign(self::CAMPAIGN_FORGOT_PASSWORD, $params);
	}

}
