<?php

namespace Redenge\OnlineUser\FrontModule\Mail;


/**
 * Description of IMail
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IMail
{

	/**
	 * Nastaví nové prostředí pro odesílání emailů
	 *
	 * @param string $multishopCode
	 * @param string $countryIso
	 */
	public function setEnvironment($multishopCode, $countryIso);

	/**
	 * @return \Redenge\OnlineUser\FrontModule\Services\Linker
	 */
	public function getLinker();

	/**
	 * Odešle aktivační email historického zákazníka
	 *
	 * @param string $email
	 * @param string $hash DoubleOptinHash
	 */
	public function sendActivationMail($email, $hash);

	/**
	 * Odešle aktivační email nové registrace
	 *
	 * @param string $email
	 * @param string $hash DoubleOptinHash
	 */
	public function sendConfirmRegistrationEmail($email, $hash);

	/**
	 * Odešle aktivační email nové registrace z Helios Orange
	 *
	 * @param string $email
	 * @param string $hash DoubleOptinHash
	 */
	public function sendConfirmRegistrationHeOEmail($email, $hash);

	/**
	 * Odešle email s žádostí o změně hesla
	 *
	 * @param string $email
	 * @param string $hash DoubleOptinHash
	 */
	public function sendForgotPasswordEmail($email, $hash);

}
