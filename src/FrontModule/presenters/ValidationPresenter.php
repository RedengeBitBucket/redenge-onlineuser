<?php

namespace Redenge\OnlineUser\FrontModule;

use Kdyby\Translation\Translator;
use Nette\Application\UI\Presenter;
use Nette\Forms\Controls\TextInput;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\Tools\ValidationHelper;


/**
 * Description of ValidationPresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class ValidationPresenter extends Presenter
{

	/**
	 * @var App
	 */
	protected $apiUser;

	/**
	 * @var Translator
	 */
	protected $translator;


	public function __construct(AppFactory $apiUserFactory, Environment $environment, Translator $translator)
	{
		$this->apiUser = $apiUserFactory->create(
			EnvironmentKeyFactory::create($environment->multishopCode, $environment->profileCode));
		$this->translator = $translator;

		parent::__construct();
	}


	public function actionValidateEmailNotExists()
	{
		$this->payload->notExists = ValidationHelper::validateEmailNotExists((new TextInput)->setValue($this->getParameter('email')),
			['api' => $this->apiUser]);
		$this->payload->message = $this->translator->translate('onlineUser.validation.email_exists');
		$this->sendPayload();
	}


	public function actionValidateIcNotExists()
	{
		$this->presenter->payload->notExists = ValidationHelper::validateIcNotExists((new TextInput)->setValue($this->presenter->getParameter('ic')),
			['api' => $this->apiUser]);
		$this->payload->message = $this->translator->translate('onlineUser.validation.ic_exists');
		$this->presenter->sendPayload();
	}


	public function actionValidateIcAresAndHeO()
	{
		$message = '';
		$isOk = ValidationHelper::validateIcNotExists((new TextInput)->setValue($this->presenter->getParameter('ic')),
			['api' => $this->apiUser]);
		if ($isOk) {
			$isOk = ValidationHelper::validateIcInAres((new TextInput)->setValue($this->presenter->getParameter('ic')));
			if (!$isOk) {
				$message = $this->translator->translate('onlineUser.validation.ic_not_in_ares');
			}
		} else {
			$message = $this->translator->translate('onlineUser.validation.ic_exists');
		}

		$this->presenter->payload->isOk = $isOk;
		$this->payload->message = $message;
		$this->presenter->sendPayload();
	}

}
