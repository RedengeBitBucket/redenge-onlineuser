<?php

namespace Redenge\OnlineUser\FrontModule;

use InvalidArgumentException;
use Kdyby\Events\EventArgsList;
use Kdyby\Events\EventManager;
use Nette\Application\UI\Form;
use Redenge\FrontModule\BasePresenter;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Components\Login\ISignInControl;
use Redenge\OnlineUser\FrontModule\Components\Login\ISignInSuperAdminControl;
use Redenge\OnlineUser\FrontModule\Exceptions\ResponseException;


/**
 * Description of LoginPresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class LoginPresenter extends BasePresenter
{

	/**
	 * @var ISignInControl @inject
	 */
	public $signFactory;

	/**
	 * @var ISignInSuperAdminControl @inject
	 */
	public $signSuperAdminFactory;

	/**
	 * @var AppFactory @inject
	 */
	public $apiUserFactory;

	/**
	 * @var Environment @inject
	 */
	public $environment;

	/**
	 * @var EventManager @inject
	 */
	public $evm;


	public function actionDefault()
	{
		
	}


	public function actionAccountActivation()
	{

	}


	public function actionLogout()
	{
		$this->user->logout(true);
		$this->redirectUrl('/');
	}


	public function actionSuperAdminLogin()
	{
		if (!$this->user->isInRole(Entity\User::ROLE_SUPER_ADMIN)) {
			$this->redirectUrl('/');
		}
	}


	public function createComponentSignIn()
	{
		$control = $this->signFactory->create();

		return $control;
	}


	public function createComponentSignInSuperAdmin()
	{
		$control = $this->signSuperAdminFactory->create();

		return $control;
	}


	public function createComponentPasswordRecovery()
	{
		$form = new Form;
		$form->getElementPrototype()->appendAttribute('class', 'onlineUser-form');
		$form->setTranslator($this->translator->domain('onlineUser.loginActivation'));

		$form->addPassword('password', 'password')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control');
		$form->addPassword('password2', 'password2')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->addRule(Form::EQUAL, '//onlineUser.validation.password_not_equal', $form['password']);
		$form->addHidden('hash', $this->getParameter('hash'));
		$form->addSubmit('send', 'complete')
			->setAttribute('class', 'ajax btn btn-default');

		$form->onSuccess[] = [$this, 'passwordRecoverySuccess'];

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['label']['container'] = 'div class="col-sm-4"';
		$renderer->wrappers['pair']['container'] = 'div class="form-group row align-items-center"';
		$renderer->wrappers['control']['container'] = 'div class="col-sm-8"';

		return $form;
	}


	public function passwordRecoverySuccess(Form $form)
	{
		try {
			$api = $this->apiUserFactory->create(EnvironmentKeyFactory::create($this->environment->multishopCode, $this->environment->profileCode));
			$api->activateAccountFromHash($form->values->hash, $form->values->password);

			$this->template->isPasswordSet = true;

			$this->flashMessage($this->translator->trans('onlineUser.loginActivation.new_password_set'), 'success');
		} catch (InvalidArgumentException $ex) {
			$this->flashMessage($this->translator->trans('onlineUser.loginActivation.form_error'), 'danger');
		} catch (ResponseException $ex) {
			$this->flashMessage($ex->getMessage(), 'danger');
		}
		
		$this->redrawControl('formWrapper');
	}


	public function renderAccountActivation()
	{
		$this->evm->dispatchEvent('Redenge\OnlineUser\FrontModule\LoginPresenter::onRender', new EventArgsList([$this->template]));
	}

}
