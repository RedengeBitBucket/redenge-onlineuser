<?php

namespace Redenge\OnlineUser\FrontModule;

use InvalidArgumentException;
use Kdyby\Events\EventArgsList;
use Kdyby\Events\EventManager;
use Nette\Application\UI\Form;
use Redenge\FrontModule\BasePresenter;
use Redenge\Application\Environment\Environment;
use Redenge\OnlineUser\FrontModule\App;
use Redenge\OnlineUser\FrontModule\AppFactory;
use Redenge\OnlineUser\FrontModule\EnvironmentKeyFactory;
use Redenge\OnlineUser\FrontModule\Exceptions\ResponseException;
use Redenge\OnlineUser\FrontModule\Exceptions\SuccessException;


/**
 * Description of RegisterPresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class RegistrationPresenter extends BasePresenter
{

	/**
	 * @var Components\Registration\IRegistrationControl @inject
	 */
	public $registrationFactory;

	/**
	 * @var AppFactory @inject
	 */
	public $apiUserFactory;

	/**
	 * @var Environment @inject
	 */
	public $environment;

	/**
	 * @var EventManager @inject
	 */
	public $evm;


	public function actionDefault()
	{
	}


	public function actionConfirmRegistration()
	{
	}


	/**
	 * Formulář s potvrzením registrace nového zákazníka
	 *
	 * @return Form
	 */
	public function createComponentConfirmRegistration()
	{
		$form = new Form;
		$form->setTranslator($this->translator->domain('onlineUser.confirmRegistration'));

		$form->addPassword('password', 'password')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control');

		$form->addPassword('password2', 'password_check')
			->setRequired('//This field is required.')
			->setAttribute('class', 'form-control')
			->addRule(Form::EQUAL, '//onlineUser.validation.password_not_equal', $form['password']);

		$form->addSubmit('send', 'complete')
			->setAttribute('class', 'ajax btn btn-default')
			->setAttribute('data-spinner-target', '.onlineUser-form');;

		$form->addHidden('hash', $this->getParameter('hash'));

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['label']['container'] = 'div class="col-sm-4"';
		$renderer->wrappers['pair']['container'] = 'div class="form-group row align-items-center"';
		$renderer->wrappers['control']['container'] = 'div class="col-sm-8"';

		$form->onSuccess[] = [$this, 'confirmRegistrationSuccess'];
		$form->onError[] = function (Form $form) {
			$this->flashMessage('onlineUser.confirmRegistration.error', 'danger');
			$this->redrawControl('formWrapper');
		};

		return $form;
	}


	public function confirmRegistrationSuccess(Form $form)
	{
		try {
			$api = $this->apiUserFactory->create(EnvironmentKeyFactory::create($this->environment->multishopCode, $this->environment->profileCode));
			$api->activateAccountFromHash($form->values->hash, $form->values->password);

			$this->template->isPasswordSet = true;

			$this->flashMessage('onlineUser.confirmRegistration.success', 'success');
			$this->redrawControl('formWrapper');
		} catch (InvalidArgumentException $e) {
			$this->redirectUrl('/');
		} catch (ResponseException $e) {
			$preException = $e->getPrevious();
            if ($preException instanceof SuccessException) {
                $this->flashMessage($preException->getMessage(), 'danger');
            } else {
				$this->flashMessage($e->getMessage(), 'danger');
			}
			$this->redrawControl('formWrapper');
		}
	}


	/**
	 * @return Control
	 */
	public function createComponentRegistration()
	{
		$control = $this->registrationFactory->create();

		return $control;
	}


	public function renderDefault()
	{
		$this->evm->dispatchEvent('Redenge\OnlineUser\FrontModule\RegistrationPresenter::onRender', new EventArgsList([$this->template]));
	}


	public function renderConfirmRegistration()
	{
		$this->evm->dispatchEvent('Redenge\OnlineUser\FrontModule\RegistrationPresenter::onRender', new EventArgsList([$this->template]));
	}

}
