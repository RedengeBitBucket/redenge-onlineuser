<?php

namespace Redenge\OnlineUser\FrontModule;

use InvalidArgumentException;
use Nette\Utils\Strings;


/**
 * Description of EnvironmentKey
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class EnvironmentKey
{

	const DELIMITER = '|';

	/**
	 * @var string
	 */
	private $key;


	/**
	 * @param array ...$args
	 */
	public function __construct( ...$args)
	{
		$this->key = implode(self::DELIMITER, array_map(function ($arg) {
			if (is_scalar($arg)) {
				return Strings::lower((string) $arg);
			}
			throw new InvalidArgumentException("Argument must be scalar.");
		}, $args));
	}


	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->key;
	}


	public function getMultishopCode()
	{
		return explode(self::DELIMITER, $this->key)[0];
	}


	public function getProfileCode()
	{
		return explode(self::DELIMITER, $this->key)[1];
	}

}
