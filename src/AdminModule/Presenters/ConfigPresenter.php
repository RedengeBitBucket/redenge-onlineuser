<?php

namespace Redenge\OnlineUser\AdminModule\Presenters;

use Redenge\DB;
use Redenge\Engine\Components\NeonConfig\INeonConfigControl;
use Redenge\Engine\Presenters\BasePresenter;
use Redenge\Engine\Tools\CacheHelper;
use Redenge\OnlineUser\AdminModule\Entity;


/**
 * Description of ConfigPresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class ConfigPresenter extends BasePresenter
{

	/**
	 * @var DB @inject
	 */
	public $db;

	/**
	 * @var INeonConfigControl @inject
	 */
	public $neonConfigFactory;


	/**
	 * Temporary fix.
	 */
	public function startup()
	{
		require_once(ENGINE_PATH . '/component/admin/interface.php');
		$admin = new \admin($this->db);
		if (!$admin->user_interface->user->login())
			$this->presenter->redirectUrl('/admin');

		parent::startup();
	}


	/**
	 * @return \Redenge\Engine\Components\Tab\TabControl
	 */
	protected function createComponentMainTab()
	{
		$control = $this->tabControl->create('OnlineUser');

		$main = $control->getTab();

		# Main
		$main->addBookmark('Konfigurace', function () {
			return $this['neonConfig'];
		})->setAction('Config:default');

		return $control;
	}


	public function createComponentNeonConfig()
	{
		$settings = (new Entity\Settings)
			->setCode('onlineUser')
			->setName('onlineUser')
			->setIdSettingsTheme(0);
		$filePath = APP_DIR . '/FrontModule/config/generated';

		$control = $this->neonConfigFactory->create($settings, $filePath);

		$control->onSuccess[] = function() {
			CacheHelper::removeConfigurator();
			$this->flashMessage('Hodnoty byly uloženy.', 'success');
			$this->redrawControl();
		};
		$control->onError[] = function() {
			$this->flashMessage('Během ukládaní nastala chyba.', 'danger');
			$this->redrawControl();
		};

		return $control;
	}


	public function actionDefault()
	{
		
	}


	public function renderDefault()
	{
		
	}

}
