<?php

namespace Redenge\OnlineUser\AdminModule\DI;

use Nette\Application\IPresenterFactory;
use Nette\DI\CompilerExtension;


class OnlineUserExtension extends CompilerExtension
{

	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();
		$builder->getDefinition($builder->getByType(IPresenterFactory::class))->addSetup(
			'setMapping',
			[['OnlineUser' => 'Redenge\OnlineUser\AdminModule\Presenters\*Presenter']]
		);
	}

}
