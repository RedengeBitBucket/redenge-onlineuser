function Cart(shop) {
	this.Shop = shop;

	LiveForm.setOptions({
		tooltipEl: '<i class="icon icon-cancel-circle text-danger" data-toggle="tooltip" data-placement="top" title=""></i>',
		messageValidPrefix: '',
		showValid: true,
		addMessageToTooltip: true
	});

	this.listener();
}

Cart.prototype.listener = function() {
	var _this = this;

	$(document).on('click', '.onlineUser-form .btnEdit', function () {
		var inputs = $(this).closest('.row').find('input, select');
		$.each(inputs, function(key, input) {
			$(input).removeAttr('readonly');
			$(input).addClass('form-control');
			$(input).removeClass('form-control-plaintext');
			$(input).attr('data-value', $(input).val());
		});
		
		$(this).addClass('d-none');
		$(this).parent().find('.btnConfirm').removeClass('d-none');
		$(this).parent().find('.btnCancel').removeClass('d-none');
	});

	$(document).on('click', '.onlineUser-form .btnCancel', function () {
		var inputs = $(this).closest('.row').find('input, select');
		$.each(inputs, function(key, input) {
			LiveForm.removeError(input);
			$(input).val($(input).attr('data-value'));
			$(input).attr('readonly', '');
			$(input).addClass('form-control-plaintext');
			$(input).removeClass('form-control');
		});
		
		$(this).parent().find('.btnEdit').removeClass('d-none');
		$(this).parent().find('.btnConfirm').addClass('d-none');
		$(this).parent().find('.btnCancel').addClass('d-none');
	});

	$(document).on('click', '.onlineUser-form .btnConfirm', function (e) {
		e.preventDefault();
		var _this = $(this);
		var url = '';
		var data = {};
		var isValid = true;
		var isChanged = false;
		var inputs = $(this).closest('.row').find('input, select');
		$.each(inputs, function(key, input) {
			isValid = Nette.validateControl(input);
			isChanged = $(input).val() !== $(input).attr('data-value');
			url = $(input).attr('inlineEdit') || url;
			data['cartInvoice-' + $(input).attr('name')] = $(input).val();
		});
		if (!isValid) {
			return;
		}
		$.nette.ajax({
			url: url,
			data: data,
			success: function (payload) {
				$.each(inputs, function(key, input) {
					$(input).attr('readonly', '');
					$(input).addClass('form-control-plaintext');
					$(input).removeClass('form-control');
					if (payload.success !== undefined && payload.success === true) {

					} else {
						$(input).val($(input).attr('data-value'));
					}
				});

				_this.parent().find('.btnEdit').removeClass('d-none');
				_this.parent().find('.btnConfirm').addClass('d-none');
				_this.parent().find('.btnCancel').addClass('d-none');
			}
		}, _this, e);
	});
	
};
