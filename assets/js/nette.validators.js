Nette.validators.RedengeOnlineUserFrontModuleToolsValidationHelper_validateIcNotExists = function (elem, arg, val) {
	var isOk = true;
	var action = arg.link;

	$.nette.ajax({
		url: action,
		data: {ic: val},
		start: function(xhr, settings) {
			$(elem).addClass('spinner');
		},
		success: function (payload) {
			if (payload.notExists) {
				LiveForm.removeError(elem);
			} else {
				LiveForm.addError(elem, payload.message);
			}
		},
		complete: function() {
			$(elem).removeClass('spinner');
		},
		validate: {
			keys: false
		}
	});

	return isOk;
};

Nette.validators.RedengeOnlineUserFrontModuleToolsValidationHelper_validateEmailNotExists = function (elem, arg, val) {
	var isOk = true;
	var action = arg.link;

	$.nette.ajax({
		url: action,
		data: {email: val},
		start: function(xhr, settings) {
			$(elem).addClass('spinner');
		},
		success: function (payload) {
			if (payload.notExists) {
				LiveForm.removeError(elem);
			} else {
				LiveForm.addError(elem, payload.message);
			}
		},
		complete: function() {
			$(elem).removeClass('spinner');
		},
		validate: {
			keys: false
		}
	});

	return isOk;
};

Nette.validators.RedengeOnlineUserFrontModuleToolsValidationHelper_validateIcAresAndHeO = function (elem, arg, val) {
	var isOk = true;
	var action = arg.link;

	$.nette.ajax({
		url: action,
		data: {ic: val},
		start: function(xhr, settings) {
			$(elem).addClass('spinner');
		},
		success: function (payload) {
			if (payload.isOk) {
				LiveForm.removeError(elem);
			} else {
				LiveForm.addError(elem, payload.message);
			}
		},
		complete: function() {
			$(elem).removeClass('spinner');
		},
		validate: {
			keys: false
		}
	});

	return isOk;
};
