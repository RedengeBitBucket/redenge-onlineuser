$.nette.ext('spinner', {
	start: function (xhr, settings) {
		if (!settings.nette || !settings.nette.el) {
			return;
		}

		this.showSpinner = settings.nette.el.is('[data-spinner-target]');
		this.target = settings.nette.el.attr('data-spinner-target');
		this.spinnerClass = settings.nette.el.attr('data-spinner-class') || 'ajax-spinner';
		this.place = settings.nette.el.attr('data-spinner-place') || 'prepend';

		if (!this.showSpinner) {
			return;
		}

		var spinner = this.createSpinner();
		if (this.place === 'prepend') {
			spinner.prependTo(this.target);
		} else if (this.place === 'append') {
			spinner.appendTo(this.target);
		}
		spinner.show();
	},
	complete: function (xhr, settings) {
		if (this.showSpinner) {
			$(this.target).find('.' + this.spinnerClass).remove();
		}
	}
},
{
	createSpinner: function () {
		return $('<div>', {
			class: this.spinnerClass,
			css: {
				display: 'none'
			}
		});
	},
	showSpinner: null,
	target: null,
	spinnerClass: null,
	place: null
});

$.nette.ext('modal-close-success', {
	success: function (payload, settings) {
		if (payload.closeModal !== undefined && payload.closeModal === true) {
			$('.modal').modal('hide');
			$('.modal').removeClass('fade');
			$('.modal-backdrop').remove();
		}
	}
});

$.nette.ext('scrollTo', {
	start: function (xhr, settings) {
		if (!settings.nette || !settings.nette.el) {
			return;
		}

		this.scroll = settings.nette.el.is('[data-scrollTo]');
		this.target = settings.nette.el.attr('data-scrollTo');
	},
	complete: function () {
		if (this.scroll) {
			$('html, body').animate({
				scrollTop: $(this.target).offset().top
			}, 250);
		}
	}
}, {
	scroll: false,
	target: null
});
