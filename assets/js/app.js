$(document).ready(function(){
	$(document).on('change', '[data-select-model]', function() {
		var form = $(this).closest('form');
		var value = $(this).val();
		var model = JSON.parse($(this).attr('data-select-model'));
		if (value in model) {
			var isChanged = false;
			for (var name in model[value]) {
				var input = form.find('[name="' + name + '"]');
				if (input.length) {
					input.val(model[value][name]);
					isChanged = true;
				}
			}
			if (isChanged) {
				window.Nette.validateForm(form[0]);
			}
		}
	});

	$('.modal.ajax').on('show.bs.modal', function (event) {
		var url = $(event.relatedTarget).attr('href');
		var content = $(this).find('.modal-content');
		var body = $(this).find('.modal-body');
		body.html('');

		$.nette.ajax({
			url: url,
			beforeSend: function() {
				var spinner = $('<div>', {
					class: 'ajax-spinner'
				});
				spinner.appendTo(content);
			},
			success: function (html) {
				body.html(html);
			},
			complete: function() {
				content.find('.ajax-spinner').remove();
			}
		});
	});
});