![Customers](http://inchoo.net/wp-content/uploads/2014/12/customer-group-featured.jpg)
# Redenge online zákazník

## Požadavky
- verze PHP 5.6 a výšší
- php extension: php_openssl.dll

## Instalace
Nejlepší možnost, jak nainstalovat balíček je přes composer  [Composer](http://getcomposer.org/):
1) Do svého composer.json přidejte odkaz na privátní repozitář satis
```sh
"repositories": [
        {
            "type": "composer",
            "url": "https://satis.redenge.biz"
        }
]
```
2) Stáhněte balíček:
```sh
$ composer require redenge/magnus
```

3) Ze složky ,,assets" zkopírujte obsah složky ,,css" do svého projektu a přidejte do hlavíčky webu:
```sh
<link rel="stylesheet" href="/css/bootstrap/bootstrap-grid.min.css" />
<link rel="stylesheet" href="/css/ico-own/style.css" />
<link rel="stylesheet" href="/css/online-user.css" />
```

3) Ze složky ,,assets" zkopírujte obsah složky ,,js" do svého projektu a přidejte do hlavíčky webu načítání js knihoven:
```sh
<script type="text/javascript" src="/js/nette.ajax.js"></script>
<script type="text/javascript" src="/js/live-form-validation.js"></script>
<script type="text/javascript" src="/js/dependentSelectBox.js"></script>
```

4) Do hlavičky webu vložte následující javascript kód:
```javascript
<script>
	$(function () {
		/*
		 * Přidat pouze, pokud v projektu ještě nepoužívám inicializaci nette.ajax
		 */
		$.nette.init();

		/*
		 * Vlastnosti pro live validace formulářů
		 */
		LiveForm.setOptions({
			messageErrorPrefix: '<i class="icon icon-cancel-circle text-danger"></i>',
			messageValidPrefix: '',
			showValid: true
		});
	});
</script>
```